#!/bin/sh

INTEGRITY_CHECK=${HUMHUB_INTEGRITY_CHECK:-1}

HUMHUB_DB_NAME=${HUMHUB_DB_NAME:-"humhub"}
HUMHUB_DB_HOST=${HUMHUB_DB_HOST:-"db"}
HUMHUB_DB_PASSWORD=${HUMHUB_DB_PASSWORD:-"password"}
HUMHUB_DB_USER=${HUMHUB_DB_USER:-"user"}

HUMHUB_NAME=${HUMHUB_NAME:-"HumHub"}
HUMHUB_EMAIL=${HUMHUB_EMAIL:-"humhub@example.com"}
HUMHUB_LANG=${HUMHUB_LANG:-"en-US"}
HUMHUB_DEBUG=${HUMHUB_DEBUG:-"false"}
HUMHUB_TIME_ZONE=${HUMHUB_TIME_ZONE:-"UTC"}

HUMHUB_ADMIN_ACCOUNT=${HUMHUB_ADMIN_ACCOUNT:-"admin"}
HUMHUB_ADMIN_EMAIL=${HUMHUB_ADMIN_EMAIL:-"admin@example.com"}
HUMHUB_ADMIN_PASSWORD=${HUMHUB_ADMIN_PASSWORD:-"test"}
HUMHUB_ADMIN_PROFILE_TITLE=${HUMHUB_ADMIN_PROFILE_TITLE:-"System Administration"}
HUMHUB_ADMIN_PROFILE_FIRSTNAME=${HUMHUB_ADMIN_PROFILE_FIRSTNAME:-"Sys"}
HUMHUB_ADMIN_PROFILE_LASTNAME=${HUMHUB_ADMIN_PROFILE_LASTNAME:-"Admin"}

HUMHUB_THEME=${HUMHUB_THEME:-"HumHub"}

CITY_NAME=${CITY_NAME:-"TestCity"}
CITY_DB_DRIVER=${CITY_DB_DRIVER:-"pgsql"}
CITY_DB_HOST=${CITY_DB_HOST:-"141.13.162.156"}
CITY_DB_NAME=${CITY_DB_NAME:-"airml"}
CITY_DB_USER=${CITY_DB_USER:-"user"}
CITY_DB_PASSWORD=${CITY_DB_PASSWORD:-"password"}
CITY_CONFIG_NAME=${CITY_CONFIG_NAME:-"city_db"}


WAIT_FOR_DB=${WAIT_FOR_DB:-"true"}
DISCUSSION_URL=${DISCUSSION_URL:-"https://ttc-sülzfeld.de"}
MEASUREMENTS_LIMIT=${MEASUREMENTS_LIMIT:-1000}
CHART_BACKGROUND_COLOR=${CHART_BACKGROUND_COLOR:-"rgba(0,110,146,0.5)"}
CHART_BORDER_COLOR=${CHART_BORDER_COLOR:-"rgba(179,181,198,1)"}
CHART_POINT_BACKGROUND_COLOR=${CHART_POINT_BACKGROUND_COLOR:-"rgba(179,181,198,1)"}
CHART_POINT_BORDER_COLOR=${CHART_POINT_BORDER_COLOR:-"#fff"}
CHART_POINT_HOVER_BACKGROUND_COLOR=${CHART_POINT_HOVER_BACKGROUND_COLOR:-"#fff"}
CHART_POINT_HOVER_BORDER_COLOR=${CHART_POINT_HOVER_BORDER_COLOR:-"rgba(179,181,198,1)"}

wait_for_db () {
  if [ "$WAIT_FOR_DB" = "false" ]; then
    return 0
  fi

  until nc -z -v -w60 $HUMHUB_DB_HOST 3306
  do
    echo "Waiting for database connection..."
    # wait for 5 seconds before check again
    sleep 5
  done
}

if [ -f "/var/www/html/protected/config/dynamic.php" ]; then
    echo "Existing installation found!"
    wait_for_db
else
    echo "No existing installation found!"
    echo "Installing source files..."
    wait_for_db

    cd /var/www/html/protected
    mkdir runtime/logs
    touch runtime/logs/app.log
    mkdir runtime/cache
    mkdir runtime/cache/hu
    chmod +x yii yii.bat
    php yii installer/write-db-config "$HUMHUB_DB_HOST" "$HUMHUB_DB_NAME" "$HUMHUB_DB_USER" "$HUMHUB_DB_PASSWORD"
    php yii installer/install-db
    php yii installer/add-db-config "$CITY_DB_DRIVER" "$CITY_DB_HOST" "$CITY_DB_NAME" "$CITY_DB_USER" "$CITY_DB_PASSWORD" "$CITY_CONFIG_NAME"
    php yii installer/write-site-config "$HUMHUB_NAME" "$HUMHUB_EMAIL" "$HUMHUB_LANG" "$HUMHUB_TIME_ZONE"
    php yii installer/create-admin-account "$HUMHUB_ADMIN_ACCOUNT" "$HUMHUB_ADMIN_EMAIL" "$HUMHUB_ADMIN_PASSWORD" "$HUMHUB_ADMIN_PROFILE_TITLE" "$HUMHUB_ADMIN_PROFILE_FIRSTNAME" "$HUMHUB_ADMIN_PROFILE_LASTNAME"
    
    php yii installer/install-module calendar
    php yii module/enable calendar
    
    php yii installer/install-module cfiles
    php yii module/enable cfiles

    php yii installer/install-module polls
    php yii module/enable polls

    php yii installer/install-module popover-vcard
    php yii module/enable popover-vcard

    php yii installer/install-module updater
    php yii module/enable updater

    php yii installer/install-module auto-patch
    php yii module/enable auto-patch

    php yii installer/install-module wiki
    php yii module/enable wiki

    php yii module/enable defectreport
    php yii module/enable discussion
    php yii module/enable Sensoren

    php yii installer/install-space "Stadtgeschehen" "Hier finden Sie alle Termine rund um $CITY_NAME." "0" "#6fdbe8"
    php yii installer/enable-space-module "Stadtgeschehen" "polls"
    php yii installer/enable-space-module "Stadtgeschehen" "calendar"
    php yii installer/enable-space-module "Stadtgeschehen" "wiki"
    php yii installer/install-space "Dokumentenaustausch" "Hier finden Sie alle öffentlichen Dokumente der Stadt $CITY_NAME." "0" "#6fdbe8"
    php yii installer/enable-space-module "Dokumentenaustausch" "cfiles"
    php yii installer/enable-space-module "Dokumentenaustausch" "wiki"

    php yii search/rebuild

    php yii installer/set-theme "$HUMHUB_THEME"

    cd /var/www/html
    chown -R www-data:www-data assets uploads protected/runtime protected/modules protected/config protected/humhub/modules/devtools protected/yii
    chmod 777 static
fi


if [ "$HUMHUB_DEBUG" = "false" ]; then
  sed -i '/YII_DEBUG/s/^\/*/\/\//' /var/www/html/index.php
#   sed -i '/YII_ENV/s/^\/*/\/\//' /var/www/html/index.php
  echo "debug disabled"
else
  sed -i '/YII_DEBUG/s/^\/*//' /var/www/html/index.php
#   sed -i '/YII_ENV/s/^\/*//' /var/www/html/index.php
  echo "debug enabled"
fi

if [ "$INTEGRITY_CHECK" != "false" ]; then
  echo "validating ..."
  cd /var/www/html/protected
  php yii integrity/run
  if [ $? -ne 0 ]; then
    echo "validation failed!"
  exit 1
  fi
else
  echo "validation skipped"
fi


# PHP-SSMTP: https://github.com/docker-library/php/issues/135#issuecomment-277199026
echo "127.0.0.1	noreply.domain.com $(hostname)" >> /etc/hosts

# Setup production .env file
cat <<EOF >/var/www/html/.env
DISCUSSION_URL="$DISCUSSION_URL"
MEASUREMENTS_LIMIT=$MEASUREMENTS_LIMIT
CHART_BACKGROUND_COLOR="$CHART_BACKGROUND_COLOR"
CHART_BORDER_COLOR="$CHART_BORDER_COLOR"
CHART_POINT_BACKGROUND_COLOR="$CHART_POINT_BACKGROUND_COLOR"
CHART_POINT_BORDER_COLOR="$CHART_POINT_BORDER_COLOR"
CHART_POINT_HOVER_BACKGROUND_COLOR="$CHART_POINT_HOVER_BACKGROUND_COLOR"
CHART_POINT_HOVER_BORDER_COLOR="$CHART_POINT_HOVER_BORDER_COLOR"
EOF

start-services
apache2-foreground