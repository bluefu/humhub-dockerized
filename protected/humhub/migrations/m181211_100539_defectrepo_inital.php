<?php

use yii\db\Migration;

/**
 * Class m181211_100539_defectrepo_inital
 */
class m181211_100539_defectrepo_inital extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181211_100539_defectrepo_inital cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181211_100539_defectrepo_inital cannot be reverted.\n";

        return false;
    }
    */
}
