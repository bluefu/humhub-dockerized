<?php

/**
 * @link https://www.humhub.org/
 * @copyright Copyright (c) 2017 HumHub GmbH & Co. KG
 * @license https://www.humhub.com/licences
 */

Yii::setAlias('@tests', dirname(__DIR__) . '/tests');

return [
    'id' => 'humhub-console',
    'controllerMap' => [
        'installer' => 'humhub\modules\installer\commands\ExtendedInstallController'
    ],
    'controllerNamespace' => 'humhub\commands',
    'components' => [
        'user' => [
            'class' => 'humhub\modules\user\components\User',
            'identityClass' => 'humhub\modules\user\models\User',
            'enableAutoLogin' => false,
            'enableSession' => false,
            'loginUrl' => ['/user/auth/login']
        ],
        'urlManager' => [
            'baseUrl' => 'http://localhost:80',
            'hostInfo' => 'http://localhost:80',
        ]
    ],
];
