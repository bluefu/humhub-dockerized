<?php
/**
 * @link https://www.humhub.org/
 * @copyright Copyright (c) 2018 Michael Riedmann
 * @license https://www.humhub.com/licences
 */
namespace humhub\modules\installer\commands;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;
use yii\helpers\ArrayHelper;
use yii\base\Exception;
use humhub\modules\user\models\User;
use humhub\modules\space\models\Space;
use humhub\modules\user\models\Password;
use humhub\modules\user\models\Group;
use humhub\modules\installer\libs\InitialData;
use humhub\libs\UUID;
use humhub\libs\DynamicConfig;
use humhub\models\Setting;

/**
 * Extended Console Install
 * based on InstallController by Luke
 *
 * @author Michael Riedmann
 */
class ExtendedInstallController extends Controller
{
    public function actionWriteDbConfig($db_host, $db_name, $db_user, $db_pass) {
        $connectionString = "mysql:host=" . $db_host . ";dbname=" . $db_name;
        $dbConfig = [
            'class' => 'yii\db\Connection',
            'dsn' => $connectionString,
            'username' => $db_user,
            'password' => $db_pass,
            'charset' => 'utf8',
        ];
        $temporaryConnection = Yii::createObject($dbConfig);
        $temporaryConnection->open();
        $config = DynamicConfig::load();
        $config['components']['db'] = $dbConfig;
        $config['params']['installer']['db']['installer_hostname'] = $db_host;
        $config['params']['installer']['db']['installer_database'] = $db_name;
        DynamicConfig::save($config);
        return ExitCode::OK;
    }

    public function actionAddDbConfig($db_driver, $db_host, $db_name, $db_user, $db_pass, $db_config_name) {
        $connectionString = $db_driver . ":host=" . $db_host . ";dbname=" . $db_name;
        $dbConfig = [
            'class' => 'yii\db\Connection',
            'dsn' => $connectionString,
            'username' => $db_user,
            'password' => $db_pass,
        ];
        $temporaryConnection = Yii::createObject($dbConfig);
        $temporaryConnection->open();
        $config = DynamicConfig::load();
        $config['components'][$db_config_name] = $dbConfig;
        DynamicConfig::save($config);
        return ExitCode::OK;
    }

    public function actionInstallDb()
    {
        $this->stdout("Install DB:\n\n", Console::FG_YELLOW);
        $this->stdout("  * Checking Database Connection\n", Console::FG_YELLOW);
        if(!$this->checkDBConnection()){
            throw new Exception("Could not connect to DB!");
        }
        $this->stdout("  * Installing Database\n", Console::FG_YELLOW);
        
        Yii::$app->cache->flush();
        // Disable max execution time to avoid timeouts during migrations
        @ini_set('max_execution_time', 0);
        \humhub\commands\MigrateController::webMigrateAll();
        DynamicConfig::rewrite();
        $this->setDatabaseInstalled();
        $this->stdout("  * Finishing\n", Console::FG_YELLOW);
        $this->setInstalled();
        return ExitCode::OK;
    }
    public function actionCreateAdminAccount($admin_user='admin', $admin_email='humhub@example.com', $admin_pass='test', $admin_profile_title='System Administration', $admin_profile_firstname='Sys', $admin_profile_lastname='Admin')
    {
        $user = new User();
        $user->username = $admin_user;
        $user->email = $admin_email;
        $user->status = User::STATUS_ENABLED;
        $user->language = '';
        if (!$user->save()) {
            throw new Exception("Could not save user");
        }
        $user->profile->title = $admin_profile_title;
        $user->profile->firstname = $admin_profile_firstname;
        $user->profile->lastname = $admin_profile_lastname;
        $user->profile->save();
        
        $password = new Password();
        $password->user_id = $user->id;
        $password->setPassword($admin_pass);
        $password->save();
        Group::getAdminGroup()->addUser($user);
        return ExitCode::OK;
    }
    public function actionWriteSiteConfig($site_name='HumHub', $site_email='humhub@example.com', $site_lang='en_US', $site_timezone='UTC'){
        $this->stdout("Install Site:\n\n", Console::FG_YELLOW);
        InitialData::bootstrap();
        Yii::$app->settings->set('name', $site_name);
        Yii::$app->settings->set('mailer.systemEmailName', $site_email);
        Yii::$app->settings->set('secret', UUID::v4());
        Yii::$app->settings->set('timeZone', /* Yii::$app->timeZone */ $site_timezone);
        Yii::$app->settings->set('defaultLanguage', $site_lang);
        $this->setInstalled();
        return ExitCode::OK;
    }

    public function actionInstallModule($moduleId) {
        $this->stdout("Install Module:\n\t".$moduleId, Console::FG_YELLOW);
        $marketplace = new \humhub\modules\admin\libs\OnlineModuleManager();
        $marketplace->install($moduleId);
        return ExitCode::OK;
    }
    
    public function actionInstallSpace($space_name, $space_description, $auto_add_new_members, $color_hash_rgb){
        $adminUser = User::findOne(['id' => 1]);
        Yii::$app->user->switchIdentity($adminUser);
        $space = new Space();
        $space->name = Yii::t("InstallerModule.controllers_ConfigController", $space_name);
        $space->description = Yii::t("InstallerModule.controllers_ConfigController", $space_description);
        $space->join_policy = Space::JOIN_POLICY_NONE;
        $space->visibility = Space::VISIBILITY_REGISTERED_ONLY;
        $space->created_by = $adminUser->id;
        $space->auto_add_new_members = $auto_add_new_members;
        $space->color = $color_hash_rgb;
        $space->save();
        return ExitCode::OK;
    }

    public function actionEnableSpaceModule($space_name, $module_id) {
        $this->stdout("Enable space module: '".$module_id."' in space '".$space_name."'\n", Console::FG_YELLOW);
        $space = Space::find()->where(['name' => $space_name])->one();
        $this->stdout("\tFound space: '".$space->name."'\n", Console::FG_YELLOW);
        foreach ($space->getAvailableModules() as $local_module_id => $module) {
            $this->stdout("\tAvailable module: '".$local_module_id."'\n", Console::FG_YELLOW);
            if ($module_id === $local_module_id && !$space->isModuleEnabled($local_module_id)) {
                $this->stdout("\tEnable module: '".$local_module_id."'\n", Console::FG_YELLOW);
                $space->enableModule($local_module_id);
                return ExitCode::OK;
            }
            $this->stdout("\t... skipped.\n\n", Console::FG_YELLOW);
        }
        return ExitCode::USAGE;
        
    }

    public function actionSetTheme($theme) {
        $theme = \humhub\modules\ui\view\helpers\ThemeHelper::getThemeByName($theme);
        if ($theme !== null) {
            $theme->activate();
        }
        \humhub\libs\DynamicConfig::rewrite();
        return ExitCode::OK;
    }

     /**
     * Sets application in installed state (disables installer)
     */
    private function setInstalled()
    {
        $config = DynamicConfig::load();
        $config['params']['installed'] = true;
        DynamicConfig::save($config);
    }
    /**
     * Sets application database in installed state
     */
    private function setDatabaseInstalled()
    {
        $config = DynamicConfig::load();
        $config['params']['databaseInstalled'] = true;
        DynamicConfig::save($config);
    }
    private function checkDBConnection()
    {
        try {
            // call setActive with true to open connection.
            Yii::$app->db->open();
            // return the current connection state.
            return Yii::$app->db->getIsActive();
        } catch (Exception $e) {
            $this->stderr($e->getMessage());
        }
        return false;
    }
}