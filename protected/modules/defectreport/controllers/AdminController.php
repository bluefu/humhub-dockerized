<?php

namespace mobilitylab\humhub\modules\defectreport\controllers;
use Yii;
use humhub\modules\admin\components\Controller;
use mobilitylab\humhub\modules\defectreport\models\TypeForm;
use mobilitylab\humhub\modules\defectreport\models\Type;
use mobilitylab\humhub\modules\defectreport\models\DeleteForm;
use mobilitylab\humhub\modules\defectreport\models\Defectrepo;
use mobilitylab\humhub\modules\defectreport\models\FilterForm;
use mobilitylab\humhub\modules\defectreport\models\DeleteDefect;
use mobilitylab\humhub\modules\defectreport\models\CommentDefect;
use mobilitylab\humhub\modules\defectreport\models\Comment;
use mobilitylab\humhub\modules\defectreport\models\ChangeStatus;
use yii\helpers\ArrayHelper;

class AdminController extends Controller
{

    /**
     * Render admin only page
     *
     * @return string
     */

    //Indexseite des Adminbereichs
    public function actionIndex()
    {
        $model2 = new CommentDefect();
        if($model2->load(Yii::$app->request->post()))
        {
            $comment = new Comment();
            $comment->comment_id = Comment::find()->max('comment_id')+1;
            $comment->text = $model2->text;
            $comment->date = date('Y-m-d H:i:s');
            $comment->user_id = Yii::$app->user->id;
            $comment->defect_id = $model2->id;
            $comment->save();
            $def = Defectrepo::find()->where(['id' => $model2->id])->one();
            $reports = Defectrepo::find()->where(['type' => $def->type])->orderBy(['id' => SORT_DESC])->all();
            return $this->render('showReportAdmin', ['reports' => $reports, 'type' => $def->type, 'model2' => $model2]);
        }
        $model = new FilterForm();
        if($model->load(Yii::$app->request->post()))
        {
            $type = Type::find()->where(['type_id' => $model->type])->one();
            $reports = Defectrepo::find()->where(['type' => $model->type])->orderBy(['id' => SORT_DESC])->all();
            return $this->render('showReportAdmin', ['reports' => $reports, 'type' => $type, 'model2' => $model2]); 
        }
        else
        {
            $items = ArrayHelper::map(Type::find()->all(),'type_id', 'name');
            return $this->render('index', [ 'model' => $model, 'items' => $items]);
        }
    }

    //anlegen eines Störungstypen
    public function actionRegister()
    {
        $model = new TypeForm();
        $type = new Type();
        if($model->load(Yii::$app->request->post()))
        {
            $type->name = $model->name;
            $type->type_id = Type::find()->max('type_id')+1;

            if($type->save())
            {
                Yii::$app->session->setFlash('typeformsubmitted');
                return $this->render('registerType', ['type' => $type]);
            }
        }
        else
        {
            return $this->render('registerType', ['model' => $model]);
        }
    }
    //löschen eines Störungstypen
    public function actionDelete()
    {
        $model = new DeleteForm();
        if($model->load(Yii::$app->request->post()))
        {
            $type = Type::find()->where(['type_id' => $model->type])->one();
            $del = Type::find()->where(['type_id' => $model->type])->one()->delete();
            Yii::$app->session->setFlash('typedeleted');
            return $this->render('dropType', ['type' => $type]);
        }
        else
        {
            $items = ArrayHelper::map(Type::find()->all(),'type_id', 'name');
            return $this->render('dropType', ['model' => $model, 'items' => $items]);
        }
       
    }
    //Störung löschen
    public function actionDeletereport()
    {
        $model2 = new CommentDefect();
        if($model2->load(Yii::$app->request->post()))
        {
            $comment = new Comment();
            $comment->comment_id = Comment::find()->max('comment_id')+1;
            $comment->text = $model2->text;
            $comment->date = date('Y-m-d H:i:s');
            $comment->user_id = Yii::$app->user->id;
            $comment->defect_id = $model2->id;
            $comment->save();
            $def = Defectrepo::find()->where(['id' => $model2->id])->one();
            $reports = Defectrepo::find()->where(['type' => $def->type])->orderBy(['id' => SORT_DESC])->all();
            return $this->render('showReportAdmin', ['reports' => $reports, 'type' => $def->type, 'model2' => $model2]);
        }
        $id = Yii::$app->request->post('id');
        $type = Yii::$app->request->post('report');
        $del = Defectrepo::find()->where(['id' => $id])->one()->delete();
        $reports = Defectrepo::find()->where(['type' => $type])->orderBy(['id' => SORT_DESC])->all();
        return $this->render('showReportAdmin',['reports' => $reports, 'model2' => $model2 ]);
    }
    //Störungs kommentieren
    public function actionComment()
    {
        $comment = new Comment();
        $model = new CommentDefect();

        if($model->load(Yii::$app->request->post()))
        {
            $comment->comment_id = Comment::find()->max('comment_id')+1;
            $comment->text = $model->text;
            $comment->date = date('Y-m-d H:i:s');
            $comment->user_id = Yii::$app->user->id;
            $comment->defect_id = $model->defect_id;

            $defect_id = Defectrepo::find()->where(['id' => $comment->defect_id])->one();


            if($comment->save())
            {
                Yii::$app->session->setFlash('commentsubmitted');
                return $this->render('commentDefect', ['comment' => $comment, 'defect_id' => $defect_id]);
            }
        }
        else
        {
            $items = ArrayHelper::map(Defectrepo::find()->all(),'id', 'id');
            return $this->render('commentDefect', ['model' => $model, 'items' => $items]);
        }
    }

    public function actionChange()
    {
        $model2 = new CommentDefect();
        if($model2->load(Yii::$app->request->post()))
        {
            $comment = new Comment();
            $comment->comment_id = Comment::find()->max('comment_id')+1;
            $comment->text = $model2->text;
            $comment->date = date('Y-m-d H:i:s');
            $comment->user_id = Yii::$app->user->id;
            $comment->defect_id = $model2->id;
            $comment->save();
            $def = Defectrepo::find()->where(['id' => $model2->id])->one();
            $reports = Defectrepo::find()->where(['type' => $def->type])->orderBy(['id' => SORT_DESC])->all();
            return $this->render('showReportAdmin', ['reports' => $reports, 'type' => $def->type, 'model2' => $model2]);
        }
        $id = Yii::$app->request->post('id');
        $type = Yii::$app->request->post('report');
        $s = Yii::$app->request->post('status');
        $def = Defectrepo::find()->where(['id' => $id])->one();
        $def->status = $s;
        $def->save();
        $reports = Defectrepo::find()->where(['type' => $type])->orderBy(['id' => SORT_DESC])->all();
        return $this->render('showReportAdmin',['reports' => $reports, 'model2' => $model2]);   
    }

    public function actionDeletecomment()
    {
        $model2 = new CommentDefect();
        if($model2->load(Yii::$app->request->post()))
        {
            $comment = new Comment();
            $comment->comment_id = Comment::find()->max('comment_id')+1;
            $comment->text = $model2->text;
            $comment->date = date('Y-m-d H:i:s');
            $comment->user_id = Yii::$app->user->id;
            $comment->defect_id = $model2->id;
            $comment->save();
            $def = Defectrepo::find()->where(['id' => $model2->id])->one();
            $reports = Defectrepo::find()->where(['type' => $def->type])->orderBy(['id' => SORT_DESC])->all();
            return $this->render('showReportAdmin', ['reports' => $reports, 'type' => $def->type, 'model2' => $model2]);
        }
        $id = Yii::$app->request->post('id');
        $type = Yii::$app->request->post('report');
        $reports = Defectrepo::find()->where(['type' => $type])->orderBy(['id' => SORT_DESC])->all();
        $del = Comment::find()->where(['comment_id' => $id])->one()->delete();
        return $this->render('showReportAdmin',['reports' => $reports, 'model2' => $model2]);
    }

}

