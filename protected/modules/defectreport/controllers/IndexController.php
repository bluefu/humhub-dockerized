<?php

namespace mobilitylab\humhub\modules\defectreport\controllers;

use humhub\components\Controller;
use mobilitylab\humhub\modules\defectreport\models\DefectForm;
use mobilitylab\humhub\modules\defectreport\models\Defectrepo;
use mobilitylab\humhub\modules\defectreport\models\Type;
use mobilitylab\humhub\modules\defectreport\models\FilterForm;
use yii;
use yii\helpers\ArrayHelper;

class IndexController extends Controller
{

    public $subLayout = "@defectreport/views/layouts/default";

    /**
     * Renders the index view for the module
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new FilterForm();
        if($model->load(Yii::$app->request->post()))
        {
            $type = Type::find()->where(['type_id' => $model->type])->one();
            $reports = Defectrepo::find()->where(['type' => $model->type])->orderBy(['id' => SORT_DESC])->all();
            return $this->render('showReport', ['reports' => $reports, 'type' => $type]); 
        }
        else
        {
            $items = ArrayHelper::map(Type::find()->all(),'type_id', 'name');
            return $this->render('index', [ 'model' => $model, 'items' => $items]);
        }
    }
    
    //Störung melden
    public function actionReport()
    {
        $model = new DefectForm();
        $defect = new Defectrepo();

        if($model->load(Yii::$app->request->post()))
        {
            $defect->id = Defectrepo::find()->max('id')+1;
            $defect->title = $model->title;
            $defect->location = $model->location;
            $defect->description = $model->description;
            $defect->type = $model->type;
            $defect->status = 0;
            $defect->date = date('Y-m-d H:i:s');
            $defect->user_id = Yii::$app->user->id;

            $type = Type::find()->where(['type_id' => $defect->type])->one();


            if($defect->save())
            {
                Yii::$app->session->setFlash('defectformsubmitted');
                return $this->render('tellDefect', ['defect' => $defect, 'type' => $type]);
            }
        }
        else
        {
            $items = ArrayHelper::map(Type::find()->all(),'type_id', 'name');
            return $this->render('tellDefect', ['model' => $model, 'items' => $items]);
        }
    }

    public function actionShowreport()
    {
        return $this->render('showReport');
    }


}

