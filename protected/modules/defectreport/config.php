<?php

use mobilitylab\humhub\modules\defectreport\Events;
use humhub\modules\admin\widgets\AdminMenu;
use humhub\widgets\TopMenu;


return [
	'id' => 'defectreport',
	'class' => 'mobilitylab\humhub\modules\defectreport\Module',
	'namespace' => 'mobilitylab\humhub\modules\defectreport',
	'events' => [
		[
			'class' => TopMenu::class,
			'event' => TopMenu::EVENT_INIT,
			'callback' => [Events::class, 'onTopMenuInit'],
		],
		[
			'class' => AdminMenu::class,
			'event' => AdminMenu::EVENT_INIT,
			'callback' => [Events::class, 'onAdminMenuInit']
		],
	],
];
