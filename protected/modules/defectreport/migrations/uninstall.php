<?php

use yii\db\Migration;

class uninstall extends Migration
{

    public function up()
    {
        $this->dropTable('defectreport_comment');
        $this->dropTable('defectreport_defectrepo');
        $this->dropTable('defectreport_type');
    }

    public function down()
    {
        echo "uninstall does not support migration down.\n";
        return false;
    }

}