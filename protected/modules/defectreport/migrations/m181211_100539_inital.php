<?php

use yii\db\Migration;

/**
 * Class m181211_100539_defectrepo_inital
 */
class m181211_100539_inital extends Migration
{
    public function up()
    {
        $this->createTable('defectreport_type', array(
            'type_id' => 'int(11)',
            'name' => 'varchar(100)',
        ),'');
        $this->addPrimaryKey('pk_defectreport_type', 'defectreport_type', 'type_id');

        $this->execute('INSERT INTO defectreport_type (type_id, name) VALUES (1, "Vandalismus")');
        $this->execute('INSERT INTO defectreport_type (type_id, name) VALUES (2, "Tiere und Ungeziefer")');
        $this->execute('INSERT INTO defectreport_type (type_id, name) VALUES (3, "Straßenlaternen")');
        $this->execute('INSERT INTO defectreport_type (type_id, name) VALUES (4, "Sensorstörungen")');
        $this->execute('INSERT INTO defectreport_type (type_id, name) VALUES (5, "Straßen und Verkehr")');
        $this->execute('INSERT INTO defectreport_type (type_id, name) VALUES (6, "Abfall")');

        $this->createTable('defectreport_defectrepo', array(
            'id' => 'int(11)',
            'location' => 'varchar(255)',
            'title' => 'varchar(255)',
            'date' => 'date',
            'type' => 'int(11)',
            'status' => 'int(11)',
            'description' => 'varchar(500)',
            'user_id' => 'int(11)',
        ),'');
        $this->addPrimaryKey('pk_defectreport_defectrepo', 'defectreport_defectrepo', 'id');

        $this->createTable('defectreport_comment', array(
            'comment_id' => 'int(11)',
            'text' => 'varchar(500)',
            'date' => 'date',
            'user_id' => 'int(11)',
            'defect_id' => 'int(11)',
        ), '');
        $this->addPrimaryKey('pk_defectreport_comment', 'defectreport_comment', 'comment_id');

        $this->addForeignKey('fk-defectreport_comment-defect_id', 'defectreport_comment', 'defect_id', 'defectreport_defectrepo', 'id', 'CASCADE');
        $this->addForeignKey('fk-defectreport_comment-user_id', 'defectreport_comment', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk-defectreport_defectrepo-user_id', 'defectreport_defectrepo', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk-defectreport_defectrepo-type', 'defectreport_defectrepo', 'type', 'defectreport_type', 'type_id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('defectreport_comment');
        $this->dropTable('defectreport_defectrepo');
        $this->dropTable('defectreport_type');
        echo "m181208_153815_inital cannot be reverted.\n";

        return false;
    }

}
