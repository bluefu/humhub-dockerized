<?php
    namespace mobilitylab\humhub\modules\defectreport\models;
    use yii;
    use yii\base\Model;

    class DeleteDefect extends Model
    {
        public $id;
        
        public function rules()
        {
            return 
            [
                [['id'], 'required'],
                [['id'], 'integer'],
            ];
        }

        public function attributeLabels()
        {
            return ['id' => 'StörungsID'];
        }
    }
?>