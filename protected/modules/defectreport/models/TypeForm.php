<?php
    namespace mobilitylab\humhub\modules\defectreport\models;
    use yii;
    use yii\base\Model;

    class TypeForm extends Model
    {
        public $name;
        
        public function rules()
        {
            return 
            [
                [['name'], 'required'],
                [['name'], 'string'],
            ];
        }

        public function attributeLabels()
        {
            return ['name' => 'Störungsart'];
        }
    }
?>