<?php
    namespace mobilitylab\humhub\modules\defectreport\models;
    use yii;
    use yii\base\Model;

    class ChangeStatus extends Model
    {
        public $status;

        
        public function rules()
        {
            return 
            [
                [['status'], 'required'],
                [['status'], 'integer'],
            ];
        }

        public function attributeLabels()
        {
            return 
            [
                'status' => 'Status',  
            ];
        }
    }
?>