<?php
    namespace mobilitylab\humhub\modules\defectreport\models;
    use yii;
    use yii\base\Model;

    class DeleteForm extends Model
    {
        public $type;
        
        public function rules()
        {
            return 
            [
                [['type'], 'required'],
                [['type'], 'string'],
            ];
        }

        public function attributeLabels()
        {
            return ['type' => 'Störungsart'];
        }
    }
?>