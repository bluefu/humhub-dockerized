<?php
    namespace mobilitylab\humhub\modules\defectreport\models;
    use yii;
    use yii\base\Model;

    class DefectForm extends Model
    {
        public $location;
        public $description;
        public $type;
        public $title;
        
        public function rules()
        {
            return 
            [
                [['location', 'type', 'title'], 'required'],
                [['location'], 'string'],
                [['description'], 'string'],
                [['title'], 'string'],
                [['type'], 'string'],
            ];
        }

        public function attributeLabels()
        {
            return 
            [
                'location' => 'Ort der Störung',
                'description' => 'Beschreibung der Störung',
                'type' => 'Störungsart',  
                'title' => 'Titel', 
            ];
        }
    }
?>