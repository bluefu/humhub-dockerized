<?php
    namespace mobilitylab\humhub\modules\defectreport\models;
    use yii;
    use yii\base\Model;

    class CommentDefect extends Model
    {
        public $text;
        public $id;
        
        public function rules()
        {
            return 
            [
                [['text'], 'string'],
                [['id'], 'integer'],
            ];
        }

        public function attributeLabels()
        {
           

            return 
            [
                'text' => 'Kommentar', 
            ];
        }
    }
?>