<?php
// Register our module assets, this could also be done within the controller
\mobilitylab\humhub\modules\defectreport\assets\Assets::register($this);

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<div class="container-fluid">
<div class="panel panel-default">
<div class="panel-heading"><strong>Störungsmeldung</strong> <?= Yii::t('DefectreportModule.base', 'Kommentierung') ?></div>
<hr>
<div class="panel-body">
   
    
    <?php if(Yii::$app->session->hasFlash('change')):?>
    <div class="row">
    <div class="col-lg-5">
      <div class="panel panel-default">

              <p><b>Neue Status-ID: </b> <?=$id->status?> </p>

          </div>
      </div>
      </div>

      <div class="alert alert-success">
          Ihre Änderung war erfolgreich. 
      </div>
      <a href="../index.php?r=defectreport%2Fadmin" class="btn btn-primary"> Zurück</a> <br> <br>
            <?php else: ?>
            <p>
    <?= Yii::t('DefectreportModule.base', 'Hier haben Sie die Möglichkeit den Status einer Störung zu ändern.') ?>
    </p>
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'defect_id')->dropDownList($items); ?>
            <?= $form->field($model, 'status')->dropDownList([0,1,2]); ?>
            <div class="form-group"><?= Html::submitButton('ändern',['class' => 'btn btn-primary', 'name' => 'contact-button'])?></div>
            <?php ActiveForm::end(); ?>

<?php endif; ?>
</div>
</div>

