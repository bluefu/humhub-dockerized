<?php
// Register module assets
\mobilitylab\humhub\modules\defectreport\assets\Assets::register($this);

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use mobilitylab\humhub\modules\defectreport\models\Comment;
use yii\helpers\Url;
use humhub\modules\user\models\User;

?> 

<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading"><strong>Störungsmeldung</strong> <?= Yii::t('DefectreportModule.base', 'Störungen') ?></div>
<hr>
        <div class="panel-body">


        <?php
        foreach($reports as $report)
        {   
            ?>
            <div class="col-sm-6">
                <div class="panel panel-default">
                        <div style="float:right;">
                        <div class="padd">
                        <div class="col-xs-12">
                        <?= Html::a('löschen', Url::to(['deletereport']), ['class' => 'btn btn-danger', 'data-method' => 'POST','data-params' => ['id' => $report->id,'report' => $report->type],]); ?>
                        </div>
                        </div>
                        </div>
                    <div class="panel-heading"><strong><?= Yii::t('SensorenModule.base', $report->title) ?></div>
                        
                    <div class="panel-body row">

                        <div class="col-xs-12">
                            <strong><?= Yii::t('DefectreportModule.base', 'Ort') ?></strong>: <?= $report->location; ?>
                        </div>
                        <div class="col-xs-12">
                            <strong><?= Yii::t('DefectreportModule.base', 'Datum') ?></strong>: <?= $report->date; ?>
                        </div>
                        <?php 
                            if($report->status == 0)
                            {
                                 $stat = "gemeldet"; ?>
                                <div class="col-xs-12">
                                <strong><?= Yii::t('DefectreportModule.base', 'Status') ?></strong>: <span style="color:red"><?= $stat;  ?></span>
                                </div>
                               <?php
                            }
                            else if($report->status == 1)
                            {
                                 $stat = "in Bearbeitung"; ?>
                                <div class="col-xs-12">
                                <strong><?= Yii::t('DefectreportModule.base', 'Status') ?></strong>: <span style="color:orange"><?= $stat;  ?></span>
                                 </div>
                               <?php
                            }
                            else
                            {
                                 $stat = "behoben"; ?>
                                <div class="col-xs-12">
                                <strong><?= Yii::t('DefectreportModule.base', 'Status') ?></strong>: <span style="color:green"><?= $stat;  ?></span>
                                </div>
                               <?php 
                            }
                        ?>
                        
                        <div class="col-xs-12">
                            <strong><?= Yii::t('DefectreportModule.base', 'Beschreibung') ?></strong>: <span style="color:black"> <?= $report->description;  ?></span>
                        </div>

                        

                        <?php
                        $status1 = 1;
                        $status2 = 2; ?>

                        <?php 
                            if($report->status == 0)
                            {?>
                                <div class="col-xs-3">
                                <div class="padd">
                                <?= Html::a('behoben', Url::to(['change']), ['class' => 'btn btn-primary', 'data-method' => 'POST','data-params' => ['id' => $report->id,'report' => $report->type,'status' => $status2],]); ?>
                                </div>
                                </div>

                                <div class="col-xs-4">
                                <div class="padd">
                                <?= Html::a('in Bearbeitung', Url::to(['change']), ['class' => 'btn btn-primary', 'data-method' => 'POST','data-params' => ['id' => $report->id,'report' => $report->type,'status' => $status1],]); ?>
                                </div>
                                </div>
                            <?php  
                            }
                            else if($report->status == 1)
                            {?>
                                <div class="col-xs-3">
                                <div class="padd">
                                <?= Html::a('behoben', Url::to(['change']), ['class' => 'btn btn-primary', 'data-method' => 'POST','data-params' => ['id' => $report->id,'report' => $report->type,'status' => $status2],]); ?>
                                </div>
                                </div>

                        
                            <?php 
                            }
                            else
                            {?>
                                
                            <?php    
                            }
                        ?>

                        

                        <?php
                        
                            $comments = Comment::find()->where(['defect_id' => $report->id])->all();
                            foreach($comments as $comment)
                            {   
                            ?>

                                <div class="col-sm-10">
                                <div class="padd">
                                    <div class="panel panel-default">
                                            <div style="float:right;">
                                            <div class="padd">
                                            <div class="col-xs-12">
                                            <?= Html::a('löschen', Url::to(['deletecomment']), ['class' => 'btn btn-danger', 'data-method' => 'POST','data-params' => ['id' => $comment->comment_id,'report' => $report->type],]); ?>
                                            </div>
                                            </div>
                                            </div>
                                        <div class="panel-heading"><strong><?= Yii::t('SensorenModule.base', 'Kommentar') ?></div>
                                        <div class="panel-body row">
                                            <div class="col-xs-12">
                                                <strong><?= Yii::t('DefectreportModule.base', 'Datum') ?></strong>: <?= $comment->date; ?>
                                            </div>
                                            <?php   $user1 = User::find()->where(['id' => $comment->user_id])->one();?>
                                            <div class="col-xs-12">
                                                <strong><?= Yii::t('DefectreportModule.base', 'Verfasser') ?></strong>: <?= $user1->username; ?>
                                            </div>
                                            <div class="col-xs-12">
                                                <strong><?= Yii::t('DefectreportModule.base', 'Kommentar') ?></strong>: <span style="color:black"><?= $comment->text; ?></span>
                                            </div>
                                            
                                            
                                        </div>
                                    </div>
                                    
                                </div>
                            </div> 
                            
                            <?php
                            }
                            ?>
                        

                        <div class="col-xs-12">
                        <div class="padd">
                        <?php $form = ActiveForm::begin(); ?>
                        <?= $form->field($model2, 'text')->label(false);?>
                        <?= $form->field($model2, 'id')->hiddenInput(['value'=> $report->id])->label(false); ?>
                        <div class="form-group"><?= Html::submitButton('kommentieren',['class' => 'btn btn-success', 'name' => 'contact-button'])?></div>
                        <?php ActiveForm::end(); ?>
                        </div>
                        </div>
                        
                    </div>
                </div>
                
            </div>
        <?php
        }
        ?>
        <div style="float:right;">
        <a href="../index.php?r=defectreport%2Fadmin" class="btn btn-primary">zurück</a>
        <br>
        </div> 
      </div> 
      </div> 
</div>   
