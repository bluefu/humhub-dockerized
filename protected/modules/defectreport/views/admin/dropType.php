<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
?>
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading"><strong>Störungsmeldung</strong> <?= Yii::t('DefectreportModule.base', 'Störungstyp löschen') ?></div>
<hr>
        <div class="panel-body">
            <?php if(Yii::$app->session->hasFlash('typedeleted')):?>
            <div class='alert alert-success'>Störungsart "<?php echo $type->name ?>" wurde gelöscht!</div>
            <a href="../index.php?r=defectreport%2Fadmin" class="btn btn-primary"> Zurück</a> <br> <br>
            <?php else: ?>
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'type')->dropDownList($items); ?>
            <div class="form-group"><?= Html::submitButton('löschen',['class' => 'btn btn-primary', 'name' => 'contact-button'])?></div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php endif;?>