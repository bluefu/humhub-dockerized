<?php
// Register our module assets, this could also be done within the controller
\mobilitylab\humhub\modules\defectreport\assets\Assets::register($this);

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<div class="container-fluid">
    <div class="panel panel-default">
<div class="panel-heading"><strong>Störungsmeldung</strong> <?= Yii::t('DefectreportModule.base', 'Kommentierung') ?></div>
<hr>
<div class="panel-body">
   
    
    <?php if(Yii::$app->session->hasFlash('commentsubmitted')):?>
    <div class="row">
    <div class="col-lg-5">
      <div class="panel panel-default">

              <p><b>Kommantar-ID: </b> <?=$comment->comment_id?> </p>
              <p><b>Störungs-ID: </b> <?=$defect_id->id?> </p>
              <p><b>Datum:</b> <?=$comment->date?> </p>
              <p><b>Verfasser-ID:</b> <?=$comment->user_id?> </p>
              <p><b>Kommantar:</b> <?=$comment->text?> </p>
          </div>
      </div>
</div>
      <hr>
      <div class="alert alert-success">
          Ihre Registierung war erfolgreich. 
      </div>
      <a href="../index.php?r=defectreport%2Fadmin" class="btn btn-primary"> Zurück</a> <br> <br>
            <?php else: ?>
            <p>
    <?= Yii::t('DefectreportModule.base', 'Hier haben Sie die Möglichkeit eine Störung zu kommentieren.') ?>
    </p>
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'defect_id')->dropDownList($items); ?>
            <?= $form->field($model, 'text'); ?>
            <div class="form-group"><?= Html::submitButton('kommentieren',['class' => 'btn btn-primary', 'name' => 'contact-button'])?></div>
            <?php ActiveForm::end(); ?>

<?php endif; ?>
</div>
</div>
</div>