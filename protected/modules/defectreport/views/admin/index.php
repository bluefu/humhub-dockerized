<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

\mobilitylab\humhub\modules\defectreport\assets\Assets::register($this);
?>

<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading"><strong>Störungsmeldung</strong> <?=Yii::t('DefectreportModule.base', 'Administration')?></div>
        <hr>
        <div class="panel-body">

            <a href="../index.php?r=defectreport%2Fadmin%2Fregister" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Störungsart anlegen</a>
            <a href="../index.php?r=defectreport%2Fadmin%2Fdelete" class="btn btn-danger"><i class="fa fa-minus" aria-hidden="true"></i> Störungsart löschen</a> <br>
        <br>

        <hr>
            <p>
                <?=Yii::t('DefectreportModule.base', 'Gemeldete Störungen einsehen.')?>
            </p>
                <?php $form = ActiveForm::begin();?>
                <?=$form->field($model, 'type')->dropDownList($items);?>
                <div class="form-group"><?=Html::submitButton('suchen', ['class' => 'btn btn-primary', 'name' => 'contact-button'])?></div>
                <?php ActiveForm::end();?>

        </div>
    </div>
</div>