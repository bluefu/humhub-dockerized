<?php
// Register module assets
\mobilitylab\humhub\modules\defectreport\assets\Assets::register($this);

use humhub\modules\user\models\User;
use mobilitylab\humhub\modules\defectreport\models\Comment;
use yii\widgets\ActiveForm;

?>
<div class="panel-heading"><strong>Störungen</strong> <?=Yii::t('DefectreportModule.base', $type->name)?></div>
<hr>
<div class="panel-body">


<?php
foreach ($reports as $report) {
    ?>
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong><?=Yii::t('SensorenModule.base', $report->title)?></div>
                    <div class="panel-body row">
                        <div class="col-xs-4">
                            <strong><?=Yii::t('DefectreportModule.base', 'Ort')?></strong>: <?=$report->location;?>
                        </div>
                        <div class="col-xs-4">
                            <strong><?=Yii::t('DefectreportModule.base', 'Datum')?></strong>: <?=$report->date;?>
                        </div>
                        <?php
if ($report->status == 0) {
        $stat = "gemeldet";?>
                                <div class="col-xs-4">
                                <strong><?=Yii::t('DefectreportModule.base', 'Status')?></strong>: <span style="color:red"><?=$stat;?></span>
                                </div>
                               <?php
} else if ($report->status == 1) {
        $stat = "in Bearbeitung";?>
                                <div class="col-xs-4">
                                <strong><?=Yii::t('DefectreportModule.base', 'Status')?></strong>: <span style="color:orange"><?=$stat;?></span>
                                 </div>
                               <?php
} else {
        $stat = "behoben";?>
                                <div class="col-xs-4">
                                <strong><?=Yii::t('DefectreportModule.base', 'Status')?></strong>: <span style="color:green"><?=$stat;?></span>
                                </div>
                               <?php
}
    ?>
                        <div class="col-xs-12">
                            <strong><?=Yii::t('DefectreportModule.base', 'Beschreibung')?></strong>: <span style="color:black"><?=$report->description;?></span>
                        </div>
                        <?php

    $comments = Comment::find()->where(['defect_id' => $report->id])->all();
    foreach ($comments as $comment) {
        ?>


                                <div class="col-sm-8">
                                <div class="padd">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><strong><?=Yii::t('SensorenModule.base', 'Kommentar')?></div>
                                        <div class="panel-body row">
                                            <div class="col-xs-6">
                                                <strong><?=Yii::t('DefectreportModule.base', 'Datum')?></strong>: <?=$comment->date;?>
                                            </div>
                                            <?php $user1 = User::find()->where(['id' => $comment->user_id])->one();?>
                                            <div class="col-xs-6">
                                                <strong><?=Yii::t('DefectreportModule.base', 'Verfasser')?></strong>: <?=$user1->username;?>
                                            </div>
                                            <div class="col-xs-12">
                                                <strong><?=Yii::t('DefectreportModule.base', 'Kommentar')?></strong>: <span style="color:black"><?=$comment->text;?></span>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>

                            <?php
}
    ?>



                    </div>
                </div>

            </div>
        <?php
}
?>
        <div style="float:right;">
        <a href="../index.php?r=defectreport%2Findex" class="btn btn-primary">zurück</a>
        <br>
        </div>

</div>

