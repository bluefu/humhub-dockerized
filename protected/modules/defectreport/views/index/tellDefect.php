<?php
// Register our module assets, this could also be done within the controller
\mobilitylab\humhub\modules\defectreport\assets\Assets::register($this);

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="panel-heading"><strong>Störungen</strong> <?=Yii::t('DefectreportModule.base', 'Meldung')?></div>
<hr>
<div class="panel-body">


    <?php if (Yii::$app->session->hasFlash('defectformsubmitted')): ?>
    <div class="row">
        <div class="col-lg-5">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p><b>Titel:</b> <?=$defect->title?> </p>
                    <p><b>Ort: </b> <?=$defect->location?> </p>
                    <p><b>Störungsart: </b> <?=$type->name?> </p>
                    <p><b>Beschreibung:</b> <?=$defect->description?> </p>
                </div>
            </div>
        <hr>
            <div class="alert alert-success">
                Ihre Registierung war erfolgreich.
            </div>
      <a href="../index.php?r=defectreport%2Findex" class="btn btn-primary"> zurück</a> <br> <br>
            <?php else: ?>
            <p>
    <?=Yii::t('DefectreportModule.base', 'Hier haben Sie die Möglichkeit, eine Störung mitzuteilen.')?>
    </p>
            <?php $form = ActiveForm::begin();?>
            <?=$form->field($model, 'title');?>
            <?=$form->field($model, 'location');?>
            <?=$form->field($model, 'type')->dropDownList($items);?>
            <?=$form->field($model, 'description');?>
            <div class="form-group"><?=Html::submitButton('anlegen', ['class' => 'btn btn-primary', 'name' => 'contact-button'])?></div>
            <?php ActiveForm::end();?>
</div>

<?php endif;?>