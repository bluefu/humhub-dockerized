<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

// Register our module assets, this could also be done within the controller
\mobilitylab\humhub\modules\defectreport\assets\Assets::register($this);

?>

<div class="panel-heading"><strong>Störungen</strong> <?=Yii::t('DefectreportModule.base', 'Übersicht')?></div>
<hr>
<div class="panel-body">

    <p>
        <?=Yii::t('DefectreportModule.base', 'Sollte Ihnen selbst eine Störung aufgefallen sein, so bitten wir Sie, diese durch den nebensthenden Button zu melden.')?>
        <div style="float:right;">
        <a href="../index.php?r=defectreport%2Findex%2Freport" class="btn btn-primary">Störung melden</a>
        <br>
        </div>
    </p>
    <p>
        <?=Yii::t('DefectreportModule.base', 'Ansonsten haben Sie die Möglichkeit, bereits gemeldete Störungen einzusehen.')?>
    </p>

    <br>
    <?php $form = ActiveForm::begin();?>
    <?=$form->field($model, 'type')->dropDownList($items);?>
    <div class="form-group"><?=Html::submitButton('suchen', ['class' => 'btn btn-primary', 'name' => 'contact-button'])?></div>
    <?php ActiveForm::end();?>

</div>


