<?php

namespace  mobilitylab\humhub\modules\discussion;

use Yii;
use yii\helpers\Url;

class Events
{
    /**
     * Defines what to do when the top menu is initialized.
     *
     * @param $event
     */
    public static function onTopMenuInit($event)
    {
        $event->sender->addItem([
            'label' => Yii::t('DiscussionModule.base', 'Discussion'),
            'icon' => '<i class="fa fa-users"></i>',
            'url' => Url::to(['/discussion/index']),
            'sortOrder' => 99999,
            'isActive' => (Yii::$app->controller->module && Yii::$app->controller->module->id == 'discussion' && Yii::$app->controller->id == 'index'),
        ]);
    }

    /**
     * Defines what to do if admin menu is initialized.
     *
     * @param $event
     */
    public static function onAdminMenuInit($event)
    {
        $event->sender->addItem([
            'label' => Yii::t('DiscussionModule.base', 'Discussion'),
            'url' => Url::to(['/discussion/admin']),
            'group' => 'manage',
            'icon' => '<i class="fa fa-users"></i>',
            'isActive' => (Yii::$app->controller->module && Yii::$app->controller->module->id == 'discussion' && Yii::$app->controller->id == 'admin'),
            'sortOrder' => 99999,
        ]);
    }
}
