<?php

namespace mobilitylab\humhub\modules\discussion\controllers;

use humhub\components\Controller;

class IndexController extends Controller
{

    public $subLayout = "@discussion/views/layouts/default";

    /**
     * Renders the index view for the module
     *
     * @return string
     */
    public function actionIndex()
    {
        $url = getenv('DISCUSSION_URL');
        if ($url == NULL) {
            $url = "https://ttc-sülzfeld.de";
        }
        return $this->render('index', [ 'url' => $url]);
    }

}

