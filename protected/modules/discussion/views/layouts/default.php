<?php

use humhub\widgets\Button;

// Register our module assets, this could also be done within the controller
\mobilitylab\humhub\modules\discussion\assets\Assets::register($this);
?>
<div class="container height-max">
    <?= $content ?>
</div>