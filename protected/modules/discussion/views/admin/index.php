<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading"><strong>Discussion</strong> <?= Yii::t('DiscussionModule.base', 'configuration') ?></div>

        <div class="panel-body">
            <p><?= Yii::t('DiscussionModule.base', 'Welcome to the admin only area.') ?></p>
        </div>
    </div>
</div>