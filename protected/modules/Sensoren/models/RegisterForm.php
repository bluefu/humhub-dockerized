<?php

namespace myCompany\humhub\modules\Sensoren\models;
use Yii;
use yii\base\Model;

class RegisterForm extends Model
{
    public $name;
    public $type;
    public $unit;
    public $remotesensor_id;
    public $description;
    public $location;

    public function rules()
    {
        return [
 
            [['name','location','type','remotesensor_id'], 'required'],
            [['description'], 'string'],
            [['location'], 'string'],
            [['type'], 'string'],
            [['name'], 'string'],
            [['remotesensor_id'], 'integer'],
            [['unit'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return ['type' => 'Sensortyp',
                'name' => 'Name',
                'description' => 'Beschreibung',
                'unit' => 'Messeinheit',
                'remotesensor_id' => 'ID des Remote-Sensors',
                'location' => 'Standort'];
    }
}
?>