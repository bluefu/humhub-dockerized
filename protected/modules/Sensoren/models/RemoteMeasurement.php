<?php

namespace myCompany\humhub\modules\Sensoren\models;
use yii\db\ActiveRecord;

class RemoteMeasurement extends ActiveRecord
{
    const DEFAULT_COUNT = 1000;

    public static function tableName()
    {
        return 'Measurement';
    }

    public static function getDb()
    {
        return \Yii::$app->city_db;
    }

    public static function findMeasurements($remote_sensor, $limit = RemoteMeasurement::DEFAULT_COUNT){
        return RemoteMeasurement::find()
                            ->where(['sensor_id' => $remote_sensor->getPrivateId()])
                            ->orderBy('time DESC')
                            ->limit($limit)
                            ->all();
    }
}
?>