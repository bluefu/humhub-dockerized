<?php

namespace myCompany\humhub\modules\Sensoren\models;
use yii\db\ActiveRecord;

class Unit extends ActiveRecord
{
    const TEMPERATURE = 1;
    const HUMIDITY = 2;
    const P10 = 3;
    const P25 = 4;

    public static function tableName()
    {
        return 'sensoren_unit';
    }

    public function attributeLabels()
    {
        return ['name' => 'Name'];
    }
}

?>