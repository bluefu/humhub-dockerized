<?php

namespace myCompany\humhub\modules\Sensoren\models;
use Yii;
use yii\base\Model;

class SensorDetailsForm extends Model
{
    public $name;
    public $description;
    public $location;
    public $type;
    public $unit;
    public $remotesensor_id;

    public function rules()
    {
        return [
 
            [['description'], 'string'],
            [['location'], 'string'],
            [['name'], 'string'],
            [['type'], 'integer'],
            [['unit'], 'string'],
            [['remotesensor_id'], 'integer']
        ];
    }
}
?>