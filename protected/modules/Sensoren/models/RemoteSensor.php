<?php

namespace myCompany\humhub\modules\Sensoren\models;
use yii\db\ActiveRecord;

class RemoteSensor extends ActiveRecord
{
    public static function tableName()
    {
        return 'Sensor';
    }

    public static function getDb()
    {
        return \Yii::$app->city_db;
    }

    public static function findByPublicId($id) {
        return RemoteSensor::find()->where(['luftdaten_id' => $id])->one();
    }

    public function getPrivateId() {
        return $this->id;
    }

    public function getPublicId() {
        return $this->luftdaten_id;
    }

    public function getPublicIdColumnName(){
        return \Yii::t('SensorenModule.base', 'ID der Luftdatenbank');
    }
}
?>