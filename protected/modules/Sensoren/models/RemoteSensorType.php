<?php

namespace myCompany\humhub\modules\Sensoren\models;
use yii\db\ActiveRecord;

class RemoteSensorType extends ActiveRecord
{
    public static function tableName()
    {
        return 'Sensortype';
    }

    public static function getDb()
    {
        return \Yii::$app->city_db;
    }
}
?>