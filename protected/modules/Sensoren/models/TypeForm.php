<?php

namespace myCompany\humhub\modules\Sensoren\models;
use Yii;
use yii\base\Model;

class TypeForm extends Model
{
    public $name;
    public $unit;

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
            [['unit'], 'string'],
            [['unit'], 'string']
        ];
    }

    public function attributeLabels()
    {
        return ['name' => 'Name des Sensortyps', 'unit' => 'Einheitsklasse'];
    }
}

?>