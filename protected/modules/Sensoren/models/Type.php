<?php

namespace myCompany\humhub\modules\Sensoren\models;
use yii\db\ActiveRecord;

class Type extends ActiveRecord
{
    public static function tableName()
    {
        return 'sensoren_type';
    }
}

?>