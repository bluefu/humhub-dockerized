<?php

namespace myCompany\humhub\modules\Sensoren\models;
use Yii;
use yii\base\Model;

class SearchForm extends Model
{
    public $name;
    public $location;

    public function rules()
    {
        return [['name', 'string'],
                ['location', 'string']
            ];
    }

    public function attributeLabels()
    {
        return ['name' => 'Name des Sensors',
                'location' => 'Standort'];
    }
}

?>