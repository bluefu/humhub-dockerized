<?php

namespace myCompany\humhub\modules\Sensoren\models;
use Yii;
use yii\base\Model;

class DeleteForm extends Model
{
    public $type;

    public function rules()
    {
        return [
 
            [['type'], 'required'],
            [['type'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return ['type' => 'Sensortyp'];
    }
    
}
?>