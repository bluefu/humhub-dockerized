<?php

namespace myCompany\humhub\modules\Sensoren\models;
use yii\db\ActiveRecord;

class Sensor extends ActiveRecord
{
    public static function tableName()
    {
        return 'sensoren_sensor';
    }

    public function attributeLabels()
    {
        return ['type' => 'Sensortyp',
                'name' => 'Name',
                'description' => 'Beschreibung',
                'unit' => 'Messeinheit',
                'remotesensor_id' => 'ID des Remote-Sensors',
                'location' => 'Standort'];
    }
}

?>