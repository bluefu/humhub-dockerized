<?php

namespace  myCompany\humhub\modules\Sensoren;

use Yii;
use yii\helpers\Url;

class Events
{
    /**
     * Defines what to do when the top menu is initialized.
     *
     * @param $event
     */
    public static function onTopMenuInit($event)
    {
        $event->sender->addItem([
            'label' => 'Sensoren',
            'icon' => '<i class="fa fa-bar-chart"></i>',
            'url' => Url::to(['/Sensoren/index']),
            'sortOrder' => 99999,
            'isActive' => (Yii::$app->controller->module && Yii::$app->controller->module->id == 'Sensoren' && Yii::$app->controller->id == 'index'),
        ]);
    }

    /**
     * Defines what to do if admin menu is initialized.
     *
     * @param $event
     */
    public static function onAdminMenuInit($event)
    {
        $event->sender->addItem([
            'label' => 'Sensoren',
            'url' => Url::to(['/Sensoren/admin']),
            'group' => 'manage',
            'icon' => '<i class="fa fa-bar-chart"></i>',
            'isActive' => (Yii::$app->controller->module && Yii::$app->controller->module->id == 'Sensoren' && Yii::$app->controller->id == 'admin'),
            'sortOrder' => 99999,
        ]);
    }
}
