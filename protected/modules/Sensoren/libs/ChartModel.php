<?php
namespace myCompany\humhub\modules\Sensoren\libs;
class ChartModel {
    public $data;
    public $data_label;
    public $xAxisLabels;
    public $unit;
    
    function __construct($xAxisLabels, $data, $data_label, $unit=null) {
        $this->xAxisLabels = $xAxisLabels;
        $this->data = $data;
        $this->data_label = $data_label;
        $this->unit = $unit;
    }
}
?>