<?php
namespace myCompany\humhub\modules\Sensoren\libs;
use myCompany\humhub\modules\Sensoren\libs\ChartModel;
use myCompany\humhub\modules\Sensoren\models\Unit;
use myCompany\humhub\modules\Sensoren\models\RemoteMeasurement;


class MeasurementUtil {
    public static function get_measurements_limit() {
        $count = getenv('MEASUREMENTS_LIMIT');
        if ($count == NULL) {
            $count = RemoteMeasurement::DEFAULT_count;
        }
        return $count;
    }

    static function build_charts($measurements, $unit = null) {
        $map_temperature = function($value) {
            return $value->temperature;
        };
        $temp = array_map($map_temperature, $measurements);
        
        $map_humidity = function($value) {
            return $value->humidity;
        };
        $humidity = array_map($map_humidity, $measurements);

        $map_p10 = function($value) {
            return $value->p10;
        };
        $p10 = array_map($map_p10, $measurements);

        $map_p25 = function($value) {
            return $value->p25;
        };
        $p25 = array_map($map_p25, $measurements);

        $map_time = function($value) {
            $date = date_create($value->time);
            $date_str = date_format($date, "d.m.y H:i");
            return $date_str;
        };
        $labels = array_map($map_time, $measurements);

        $temp_chart = new ChartModel($labels, $temp, \Yii::t('SensorenModule.base', 'Temperatur'));
        $humidity_chart = new ChartModel($labels, $humidity, \Yii::t('SensorenModule.base', 'Feuchtigkeit'));
        $p10_chart = new ChartModel($labels, $p10, \Yii::t('SensorenModule.base', 'P10'));
        $p25_chart = new ChartModel($labels, $p25, \Yii::t('SensorenModule.base', 'P25'));
        
        $charts = [];
        $charts = MeasurementUtil::add_chart($charts, $temp_chart, Unit::TEMPERATURE, $unit);
        $charts = MeasurementUtil::add_chart($charts, $humidity_chart, Unit::HUMIDITY, $unit);
        $charts = MeasurementUtil::add_chart($charts, $p10_chart, Unit::P10, $unit);
        $charts = MeasurementUtil::add_chart($charts, $p25_chart, Unit::P25, $unit);
        return $charts;
    }

    static function add_chart($charts, $chart, $expected_unit, $unit) {
        if(max($chart->data) === null) {
            return $charts;
        }
        if($unit !== null && $expected_unit != $unit->unit_id) {
            return $charts;
        }
        array_push($charts, $chart);
        return $charts;
    }
}
?>