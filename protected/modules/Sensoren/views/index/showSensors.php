<?php

use \humhub\compat\CHtml;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use myCompany\humhub\modules\Sensoren\models\Favorites;

\myCompany\humhub\modules\Sensoren\assets\Assets::register($this);

?>
<div class="container-fluid">


    <div class="panel-heading"><strong>Sensoren</strong> <?= Yii::t('SensorenModule.base', $type->name) ?></div>
    <hr>
    <div class="panel-body">
        <a href="../index.php?r=Sensoren%2Findex" class="btn btn-primary">Zurück</a>
        <hr>
        <div class="row">

        <?php if(Yii::$app->session->hasFlash('favorite')): ?>
        <div class="row">
    <div class="col-lg-5">
        <div class="panel panel-default">

            <div class="alert alert-success">
  
            Sie haben den Sensor erfolgreich favorisiert!
        </div>

<?php else: ?>
        
        <?php
        foreach($sensors as $sensor)
        {   
            ?>
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong><?= Yii::t('SensorenModule.base', 'Name') ?></strong>: <?= $sensor->name; ?></div>
                    <div class="panel-body row">
                        <div class="col-xs-4">
                            <strong><?= Yii::t('SensorenModule.base', 'Typ') ?></strong>: <?= $type->name; ?>
                        </div>
                        <div class="col-xs-4">
                            <strong><?= Yii::t('SensorenModule.base', 'Einheit') ?></strong>: <?= $sensor->unit; ?>
                        </div>
                        <div class="col-xs-4">
                            <strong><?= Yii::t('SensorenModule.base', 'Remote-Sensor ID') ?></strong>: <?= $sensor->remotesensor_id; ?>
                        </div>
                        <div class="col-xs-12">
                            <strong><?= Yii::t('SensorenModule.base', 'Ort') ?></strong>: <?= $sensor->location; ?>
                        </div>
                        <div class="col-xs-12">
                            <strong><?= Yii::t('SensorenModule.base', 'Beschreibung') ?></strong>: <?= $sensor->description; ?>
                        </div>
                        
                        <div class="col-xs-12">
                        <hr>
                            <?= Html::a('auswählen', Url::to(['data']), ['class' => 'btn btn-primary', 'data-method' => 'POST','data-params' => ['id' => $sensor->sensor_id],]); ?>
                            <?php   
                             $user = Yii::$app->user->id;
                             $fav = Favorites::find()->where(['user_id' => $user])->andWhere(['sensor_id' => $sensor->sensor_id])->one();
                             if($fav == NULL)
                             { ?>
                                <?= Html::a('favorisieren', Url::to(['favorite']), ['class' => 'btn btn-success', 'data-method' => 'POST','data-params' => ['id' => $sensor->sensor_id],]); ?>
                            <?php } 
                            else{
                               ?>
                                <?= Html::a('nicht mehr favorisieren', Url::to(['notfavorite']), ['class' => 'btn btn-danger', 'data-method' => 'POST','data-params' => ['id' => $sensor->sensor_id],]); ?>
                                <i class="fa fa-star fa-2x"></i>
                                <?php
                            }
                            ?>
                        </div>
                        
                    </div>
                </div>
                
            </div>
        <?php
        }
        ?>
        </div>
    </div>
</div>

<?php endif ?>
