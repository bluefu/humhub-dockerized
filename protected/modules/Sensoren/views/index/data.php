<?php

use humhub\widgets\Button;
use yii\helpers\Html;
use myCompany\humhub\modules\Sensoren\models\RemoteSensor;
use dosamigos\chartjs\ChartJs;




// Register our module assets, this could also be done within the controller
\myCompany\humhub\modules\Sensoren\assets\Assets::register($this);
?>
<div class="container-fluid">
    <div class="row panel-heading">
        <div class="col-sm-8">
            <div><strong>Sensoren</strong> <?= Yii::t('SensorenModule.base', 'Daten') ?></div>
        </div>
        <div class="col-sm-4 text-right">
            <?=  \yii\helpers\Html::a(Yii::t('SensorenModule.base', 'Zurück'), Yii::$app->request->referrer, ['class' => 'btn btn-primary']); ?>
        </div>
        
    </div>
    
    <div class="panel-heading row">
        <div class="col-xs-12">
            <strong><?= Yii::t('SensorenModule.base', 'Name') ?></strong>: <?= $sensor->name; ?>
        </div>
        <div class="col-xs-12">
            <strong><?= Yii::t('SensorenModule.base', 'Ort') ?></strong>: <?= $sensor->location; ?>
        </div>
        <div class="col-xs-12">
            <strong><?= Yii::t('SensorenModule.base', 'Beschreibung') ?></strong>: <?= $sensor->description; ?>
        </div>
        <div class="col-xs-12">
            <strong><?= Yii::t('SensorenModule.base', 'Typ') ?></strong>: <?= $type->name; ?> (<?= $remote_sensor_type->name; ?>)
        </div>
        <div class="col-xs-12">
            <strong><?= $remote_sensor->getPublicIdColumnName(); ?></strong>: <?= $remote_sensor->getPublicId(); ?>
        </div>
    </div>
    <hr>
    <div class="panel-body">
        <div class="row">
            <?php
                foreach ($charts as $c){
                    $data_label = $c->data_label;
                    if($sensor->unit !== null) {
                        $data_label = $data_label." (".$sensor->unit.")";
                    }
                    $type = 'line';
                    $data = [
                        'labels' => $c->xAxisLabels,
                        'datasets' => [
                            [
                                'label' => $data_label,
                                'backgroundColor' => getenv('CHART_BACKGROUND_COLOR'),
                                'borderColor' => getenv('CHART_BORDER_COLOR'),
                                'pointBackgroundColor' => getenv('CHART_POINT_BACKGROUND_COLOR'),
                                'pointBorderColor' => getenv('CHART_POINT_BORDER_COLOR'),
                                'pointHoverBackgroundColor' => getenv('CHART_POINT_HOVER_BACKGROUND_COLOR'),
                                'pointHoverBorderColor' => getenv('CHART_POINT_HOVER_BORDER_COLOR'),
                                'data' => $c->data
                            ]
                        ]
                            ];
            ?>
            <div class="visible-lg-block col-xs-12">
                <?= ChartJs::widget([
                    'type' => $type,
                    'options' => [
                        'height' => 50,
                        'width' => 100
                    ],
                    'data' => $data
                ]);
                ?>
            </div>
            <div class="visible-md-block col-xs-12">
                <?= ChartJs::widget([
                    'type' => $type,
                    'options' => [
                        'height' => 50,
                        'width' => 100
                    ],
                    'data' => $data
                ]);
                ?>
            </div>
            <div class="visible-sm-block col-xs-12">
                <?= ChartJs::widget([
                    'type' => $type,
                    'options' => [
                        'height' => 50,
                        'width' => 75
                    ],
                    'data' => $data
                ]);
                ?>
            </div>
            <div class="visible-xs-block col-xs-12">
                <?= ChartJs::widget([
                    'type' => $type,
                    'options' => [
                        'height' => 50,
                        'width' => 50
                    ],
                    'data' => $data
                ]);
                ?>
            </div>
            <?php } ?>
        
        </div>
    </div>
</div>
