<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use myCompany\humhub\modules\Sensoren\models\Type;
use myCompany\humhub\modules\Sensoren\models\Favorites;
use myCompany\humhub\modules\Sensoren\models\Sensor;

?>

<div class="container-fluid">
<div class="panel panel-default">
<div class="panel-heading"><strong>Sensoren </strong><?= Yii::t('SensorenModule.base', 'Favoriten')?> </div>
<hr>

<div class="panel-body">
<a href="../index.php?r=Sensoren%2Findex" class="btn btn-primary">Zurück</a>
<hr>

<?php  
foreach($favorites as $favorite)
        {   
            ?>
            <?php $sensor = Sensor::find()->where(['sensor_id' => $favorite->sensor_id])->one(); ?>
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong><?= Yii::t('SensorenModule.base', 'Name') ?></strong>: <?= $sensor->name; ?></div>
                    <div class="panel-body row">
                        <div class="col-xs-4">
                            <?php $type = Type::find()->where(['type_id' => $sensor->type])->one() ?>
                            <strong><?= Yii::t('SensorenModule.base', 'Typ') ?></strong>: <?= $type->name; ?>
                        </div>
                        <div class="col-xs-4">
                            <strong><?= Yii::t('SensorenModule.base', 'Einheit') ?></strong>: <?= $sensor->unit; ?>
                        </div>
                        <div class="col-xs-4">
                            <strong><?= Yii::t('SensorenModule.base', 'Remote-Sensor ID') ?></strong>: <?= $sensor->remotesensor_id; ?>
                        </div>
                        <div class="col-xs-12">
                            <strong><?= Yii::t('SensorenModule.base', 'Ort') ?></strong>: <?= $sensor->location; ?>
                        </div>
                        <div class="col-xs-12">
                            <strong><?= Yii::t('SensorenModule.base', 'Beschreibung') ?></strong>: <?= $sensor->description; ?>
                        </div>
                        
                        <div class="col-xs-12">
                        <hr>
                            <?= Html::a('auswählen', Url::to(['data']), ['class' => 'btn btn-primary', 'data-method' => 'POST','data-params' => ['id' => $sensor->sensor_id],]); ?>
                            </div>
                        
                        </div>
                    </div>
                    
                </div>
            <?php
            }
            ?>