<?php

use humhub\widgets\Button;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


// Register our module assets, this could also be done within the controller
\myCompany\humhub\modules\Sensoren\assets\Assets::register($this);

$displayName = (Yii::$app->user->isGuest) ? Yii::t('SensorenModule.base', 'Guest') : Yii::$app->user->getIdentity()->displayName;


// Add some configuration to our js module
$this->registerJsConfig("Sensoren", [
    'username' => (Yii::$app->user->isGuest) ? $displayName : Yii::$app->user->getIdentity()->username,
    'text' => [
        'hello' => Yii::t('SensorenModule.base', 'Hi there {name}!', ["name" => $displayName])
    ]
])

?>

<div class="panel-heading"><strong>Sensoren</strong> <?= Yii::t('SensorenModule.base', 'Übersicht') ?></div>
<hr>
<div class="panel-body">
    <?= Yii::t('SensorenModule.base', 'Hier können Sie nach einem bestimmen Sensor suchen: ') ?>
               
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($search, 'name'); ?>
    <?= $form->field($search, 'location'); ?>
    <?= Html::submitButton('suchen', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
    <?php ActiveForm::end(); ?>
    <hr>
    <?= Yii::t('SensorenModule.base', 'Hier können Sie die Sensoren filtern: ') ?>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'type')->dropDownList($items); ?>
    <?= Html::submitButton('suchen', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
    <?php ActiveForm::end(); ?>


    <hr>

    <a href="../index.php?r=Sensoren%2Findex%2Fshowfavorites" class="btn btn-primary">Favoriten anzeigen</a>

</div>







