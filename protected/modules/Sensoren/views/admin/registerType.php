<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<div class="container-fluid">
<div class="panel panel-default">
<div class="panel-body">

<?php if (Yii::$app->session->hasFlash('typeFormSubmitted')): ?>
 
 <div class="row">
  
    <div class="col-lg-5">
  
        <div class="panel panel-default">
  
            <div class="panel-heading"><strong>Registrierung</strong> <?= Yii::t('SensorenModule.base', 'Übersicht') ?></div>

            <hr>
  
        </div>

        <hr>
  
        <div class="alert alert-success">
  
            Sensortyp "<?= Yii::t('SensorenModule.base', $type->name) ?>" (<?= Yii::t('SensorenModule.base', 'Einheit') ?>: <?= Yii::t('SensorenModule.base', $unit->name) ?>) wurde  erfolgreich angelegt
  
        </div>

        <a href="../index.php?r=Sensoren%2Fadmin" class="btn btn-primary">Zurück</a>
  
    </div>
  
 </div>
  
<?php else: ?>

<div class="panel-heading"><strong>Sensoren </strong><?= Yii::t('SensorenModule.base', 'Neuen Sensortyp anlegen')?> </div>
<hr>
  
 <div class="row">
  
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($model, 'unit')->dropDownList($items) ; ?>
                    <?= $form->field($model, 'name'); ?>

                   <div class="form-group">
  
                        <?= Html::submitButton('anlegen', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
  
                    </div>
  
                <?php ActiveForm::end(); ?>

  
            </div>
  
        </div>
        </div>
        </div>
  
 <?php endif; ?>


