<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<div class="container-fluid">
<div class="panel panel-default">
<div class="panel-body">

<?php if (Yii::$app->session->hasFlash('typedeleted')): ?>
 
 <div class="row">
  
    <div class="col-lg-5">
  
        <div class="panel panel-default">
  
            <div class="panel-heading"><strong>Sensoren</strong> <?= Yii::t('SensorenModule.base', 'Bestätigung') ?></div>

            <hr>
  
        </div>

        <hr>
  
        <div class="alert alert-success">
        Sensortyp "<?php echo $type->name ?>" wurde gelöscht.
        </div>

        <a href="../index.php?r=Sensoren%2Fadmin" class="btn btn-primary">Zurück</a>
  
    </div>
  
 </div>
  
<?php else: ?>

<div class="panel-heading"><strong>Sensoren </strong><?= Yii::t('SensorenModule.base', 'Sensortyp löschen')?> </div>
<hr>
  
 <div class="row">
  
            <div class="col-lg-5">
  
                <?php $form = ActiveForm::begin(); ?>
  
                    <?= $form->field($model, 'type')->dropDownList($items); ?>

                   <div class="form-group">
  
                        <?= Html::submitButton('löschen', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
  
                    </div>
  
                <?php ActiveForm::end(); ?>

  
            </div>
  
        </div>
        </div>
        </div>
  
 <?php endif; ?>


