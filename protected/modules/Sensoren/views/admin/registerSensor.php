<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<div class="container-fluid">
<div class="panel panel-default">
<div class="panel-heading"><strong>Sensoren </strong><?= Yii::t('SensorenModule.base', 'Registrierung')?> </div>
<hr>

<div class="panel-body">

<?php if (Yii::$app->session->hasFlash('registerFormSubmitted')): ?>
 <div class="row">
    <div class="col-lg-5">
        <div class="panel panel-default">
            <div class="panel-heading"><strong>Registrierung</strong> <?= Yii::t('SensorenModule.base', 'Übersicht') ?></div>
            <hr>
            <div class="panel-body">
                <p><b>Name:</b> <?=$sensor->name?> </p>
                <p><b>Beschreibung:</b> <?=$sensor->description?> </p>
                <p><b>Typ: </b> <?=$type->name?> </p>
                <p><b>Ort: </b> <?=$sensor->location?> </p>
                <p><b>Messeinheit: </b> <?=$sensor->unit?> </p>
            </div>
        </div>
        <hr>
        <div class="alert alert-success">
  
            Ihre Registierung war erfolgreich. 
        </div>
        <a href="../index.php?r=Sensoren%2Fadmin" class="btn btn-primary">Zurück</a>
    </div>
 </div>
  
<?php else: ?>
  
 <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($model, 'name'); ?>
                    <?= $form->field($model, 'remotesensor_id'); ?>
                    <?= $form->field($model, 'description'); ?>
                    <?= $form->field($model, 'type')->dropDownList($items) ; ?>
                    <?= $form->field($model, 'location'); ?>
                    <?= $form->field($model, 'unit'); ?>
                   <div class="form-group">
                        <?= Html::submitButton('registrieren', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        </div>
        </div>
  
 <?php endif; ?>


