<?php 
use  myCompany\humhub\modules\Sensoren\models\Type;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="container-fluid">
    <div class="panel panel-default">
        <div class="panel-heading"><strong>Sensoren</strong> <?= Yii::t('SensorenModule.base', 'Administration') ?></div>
        <hr>

        <div class="panel-body">
            <a href="../index.php?r=Sensoren%2Fadmin%2Fregister" class="btn btn-primary">
            <i class="fa fa-plus" aria-hidden = "true"></i> Sensor registrieren </a>
            <a href="../index.php?r=Sensoren%2Fadmin%2Ftype" class="btn btn-primary">
            <i class="fa fa-plus" aria-hidden = "true"></i> Sensortyp anlegen</a>
            <a href="../index.php?r=Sensoren%2Fadmin%2Fdroptype" class="btn btn-danger">
            <i class="fa fa-minus" aria-hidden = "true"></i> Sensortyp löschen</a>

            <hr>

            <?php

        foreach($sensors as $sensor)
        {   
            ?>
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong><?= Yii::t('SensorenModule.base', 'Name') ?></strong>: <?= $sensor->name; ?></div>
                    <div class="panel-body row">
                        <div class="col-xs-4">
                        <?php $type = Type::find()->where(['type_id' => $sensor->type])->one(); ?>
                            <strong><?= Yii::t('SensorenModule.base', 'Typ') ?></strong>: <?= $type->name; ?>
                        </div>
                        <div class="col-xs-4">
                            <strong><?= Yii::t('SensorenModule.base', 'Einheit') ?></strong>: <?= $sensor->unit; ?>
                        </div>
                        <div class="col-xs-4">
                            <strong><?= Yii::t('SensorenModule.base', 'Remote-Sensor ID') ?></strong>: <?= $sensor->remotesensor_id; ?>
                        </div>
                        <div class="col-xs-12">
                            <strong><?= Yii::t('SensorenModule.base', 'Ort') ?></strong>: <?= $sensor->location; ?>
                        </div>
                        <div class="col-xs-12">
                            <strong><?= Yii::t('SensorenModule.base', 'Beschreibung') ?></strong>: <?= $sensor->description; ?>
                        </div>
                        
                        <div class="col-xs-12">
                        <hr>
                            <?= Html::a('löschen', Url::to(['deletesensor']), ['class' => 'btn btn-danger', 'data-method' => 'POST','data-params' => ['id' => $sensor->sensor_id],]); ?>
                        </div>
                        
                    </div>
                </div>
                
            </div>
        <?php
        }
        ?>
        </div>
    </div>
</div>
        </div>
    </div>
</div>