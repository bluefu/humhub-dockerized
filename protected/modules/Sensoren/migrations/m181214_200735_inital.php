<?php

use yii\db\Migration;
use myCompany\humhub\modules\Sensoren\models\Unit;

/**
 * Class m181214_200735_sensoren_inital
 */
class m181214_200735_inital extends Migration
{
    public function up()
    {
        $this->createTable('sensoren_unit', array(
            'unit_id' => 'int(11)',
            'name' => 'varchar(100)'
        ));
        $this->addPrimaryKey('pk_sensoren_unit', 'sensoren_unit', 'unit_id');

        $this->createTable('sensoren_type', array(
            'type_id' => 'int(11)',
            'name' => 'varchar(100)',
            'unit_id' => 'int(11)'
        ), '');

        $this->addPrimaryKey('pk_sensoren_type', 'sensoren_type', 'type_id');
        $this->addForeignKey(
            'fk-sensoren_sensor_type-unit',
            'sensoren_type',
            'unit_id',
            'sensoren_unit',
            'unit_id',
            'CASCADE');

        $this->createTable('sensoren_sensor', array(
            'sensor_id' => 'bigint(20)',
            'name' => 'varchar(255)',
            'description' => 'varchar(500)',
            'type' => 'int(11)',
            'location' => 'varchar(255)',
            'unit' => 'varchar(500)',
            'remotesensor_id' => 'bigint(20)', 
        ), '');

        $this->addPrimaryKey('pk_sensoren_sensor', 'sensoren_sensor', 'sensor_id');

        $this->addForeignKey(
            'fk-sensoren_sensor-type',
            'sensoren_sensor',
            'type',
            'sensoren_type',
            'type_id',
            'CASCADE');

        $this->createTable('sensoren_favorites', array(
            'sensor_id' => 'bigint(20)',
            'user_id' => 'int(11)',
        ), '');

        $this->addPrimaryKey('pk_sensoren_favorites', 'sensoren_favorites', 'sensor_id, user_id');

        $this->addForeignKey(
            'fk-sensoren_favorites-sensor_id',
            'sensoren_favorites',
            'sensor_id',
            'sensoren_sensor',
            'sensor_id',
            'CASCADE');
    
            $this->addForeignKey(
            'fk-sensoren_favorites-user_id',
            'sensoren_favorites',
            'user_id',
            'user',
            'id',
            'CASCADE');

        $this->execute('INSERT INTO sensoren_unit (unit_id, name) VALUES ('.Unit::TEMPERATURE.', "Temperature")');
        $this->execute('INSERT INTO sensoren_unit (unit_id, name) VALUES ('.Unit::HUMIDITY.', "Humidity")');
        $this->execute('INSERT INTO sensoren_unit (unit_id, name) VALUES ('.Unit::P10.', "PM10")');
        $this->execute('INSERT INTO sensoren_unit (unit_id, name) VALUES ('.Unit::P25.', "PM2.5")');

        $this->execute('INSERT INTO sensoren_type (type_id, name, unit_id) VALUES (1, "DHT22 (Temp)", '.Unit::TEMPERATURE.')');
        $this->execute('INSERT INTO sensoren_type (type_id, name, unit_id) VALUES (2, "DHT22 (Hum)", '.Unit::HUMIDITY.')');
        $this->execute('INSERT INTO sensoren_type (type_id, name, unit_id) VALUES (3, "SDS011 (PM10)", '.Unit::P10.')');
        $this->execute('INSERT INTO sensoren_type (type_id, name, unit_id) VALUES (4, "SDS011 (PM2.5)", '.Unit::P25.')');

        $this->execute('INSERT INTO sensoren_sensor (sensor_id, name, description, type, location, unit, remotesensor_id) VALUES (1, "BA_Z_DHT22_TEMP", "Temperatursensor in der Innenstadt Bambergs", 1, "Bamberg Innenstadt", "°C", 5270)');
        $this->execute('INSERT INTO sensoren_sensor (sensor_id, name, description, type, location, unit, remotesensor_id) VALUES (2, "BA_Z_DHT22_HUM", "Feuchtigkeitssensor in der Innenstadt Bambergs", 2, "Bamberg Innenstadt", "%", 5270)');
        $this->execute('INSERT INTO sensoren_sensor (sensor_id, name, description, type, location, unit, remotesensor_id) VALUES (3, "CO_ANG_SDS011_PM10", "Schillerplatz, Coburg (PM10)", 3, "Coburg Anger", "µg/m³", 15883)');
        $this->execute('INSERT INTO sensoren_sensor (sensor_id, name, description, type, location, unit, remotesensor_id) VALUES (4, "CO_ANG_SDS011_PM25", "Feinstaubsensor am Anger, Schillerplatz, Coburg (PM2.5)", 4, "Coburg Anger", "µg/m³", 15883)');
    }

    public function down()
    {
        echo "m181208_134003_Sensoren_inital does not support migration down.\n";

        return true;
    }
}
