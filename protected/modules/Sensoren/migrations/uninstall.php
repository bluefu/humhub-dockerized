<?php

use yii\db\Migration;

class uninstall extends Migration
{

    public function up()
    {
        $this->dropTable('sensoren_favorites');
        $this->dropTable('sensoren_sensor');
        $this->dropTable('sensoren_type');
        $this->dropTable('sensoren_unit');
    }

    public function down()
    {
        echo "uninstall does not support migration down.\n";
        return false;
    }

}