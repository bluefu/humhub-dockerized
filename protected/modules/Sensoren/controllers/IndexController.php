<?php


namespace myCompany\humhub\modules\Sensoren\controllers;
use yii\helpers\ArrayHelper;
use humhub\components\Controller;
use myCompany\humhub\modules\Sensoren\models\SearchForm;
use myCompany\humhub\modules\Sensoren\models\FilterForm;
use myCompany\humhub\modules\Sensoren\models\Sensor;
use myCompany\humhub\modules\Sensoren\models\Type;
use myCompany\humhub\modules\Sensoren\models\RemoteSensor;
use myCompany\humhub\modules\Sensoren\models\RemoteSensorType;
use myCompany\humhub\modules\Sensoren\models\RemoteSensorMeasurement;
use myCompany\humhub\modules\Sensoren\models\RegisterForm;
use myCompany\humhub\modules\Sensoren\models\SensorDetailsForm;
use myCompany\humhub\modules\Sensoren\libs\MeasurementUtil;
use Yii;
use yii\console\Exception;
use myCompany\humhub\modules\Sensoren\models\RemoteMeasurement;
use myCompany\humhub\modules\Sensoren\models\Unit;
use myCompany\humhub\modules\Sensoren\models\Favorites;


class IndexController extends Controller
{

    public $subLayout = "@Sensoren/views/layouts/default";

    /**
     * Renders the index view for the module
     *
     * @return string
     */

    public function actionIndex()
    { 
        $model = new FilterForm();
        $search = new SearchForm();

        if($model->load(Yii::$app->request->post()))
        {
             
            $type = Type::find()->where(['type_id' => $model->type])->one();
            $sensors = Sensor::find()->where(['type' => $model->type])->all();
            return $this->render('showSensors', ['sensors' => $sensors, 'type' => $type]);
        }
        else if($search->load(Yii::$app->request->post()))
        {
            if($search->name != NULL && $search->location == NULL)
            {
                $sensors = Sensor::find()->where(['like', 'name', $search->name])->all();
                return $this->render('showSearchResult', ['sensors' => $sensors]);
            }
            else if($search->name == NULL && $search->location != NULL)
            {
                $sensors = Sensor::find()->where(['location' => $search->location])->all();
                return $this->render('showSearchResult', ['sensors' => $sensors]);
            }
            else if($search->name != NULL && $search->location != NULL)
            {
                $sensors = Sensor::find()->where(['like', 'name', $search->name])->andWhere(['location' => $search->location])->all();
                return $this->render('showSearchResult', ['sensors' => $sensors]);
            }
            else if($search->name == NULL && $search->location == NULL)
            {
                Yii::$app->session->setFlash('noInput');
                return $this->render('showSearchResult');
            }
        }
        else
        {
            $items = ArrayHelper::map(Type::find()->all(), 'type_id', 'name');
            return $this->render('index', ['model' => $model, 'items' => $items, 'search' => $search]);
        }
    }

    public function actionSearch()
    {
            $model = new SearchForm();
            return $this->render('index', ['model' => $model]);
    }

    public function actionData()
    {

        $sensor = new SensorDetailsForm();
        $id = Yii::$app->request->post('id');
        $sensor = Sensor::find()->where(['sensor_id' => $id])->one();
        $type = Type::find()->where(['type_id' => $sensor->type])->one();
        $unit = Unit::find()->where(['unit_id' => $type->unit_id])->one();
        $remote_sensor = RemoteSensor::findByPublicId($sensor->remotesensor_id);
        $remote_sensor_type = RemoteSensorType::find()->where(['id' => $remote_sensor->sensortype_id])->one();
        $limit = MeasurementUtil::get_measurements_limit();
        $measurements = RemoteMeasurement::findMeasurements($remote_sensor, $limit);
        $reverse_measurements = array_reverse($measurements);
        $charts = MeasurementUtil::build_charts($reverse_measurements, $unit);
        return $this->render('data', ['sensor' => $sensor, 'type' => $type, 'remote_sensor' => $remote_sensor, 'remote_sensor_type' => $remote_sensor_type, 'measurements' => $measurements, 'charts' => $charts]);

    }

    public function actionFavorite()
    {
        $id = Yii::$app->request->post('id');
        $user = Yii::$app->user->id;

        $fav = new Favorites();
        $fav->sensor_id = $id;
        $fav->user_id = $user;


        if($id != NULL && $user != NULL)
        {
            $fav->save();
        }
            Yii::$app->session->setFlash('favorite');
            return $this->render('showSearchResult');
    }
    
    public function actionShowfavorites()
    {
        $user = Yii::$app->user->id;

        $favorites = Favorites::find()->where(['user_id' => $user])->all();
        return $this->render('showFavorites', ['favorites' => $favorites]);
    }

    public function actionNotfavorite()
    {
        $id = Yii::$app->request->post('id');
        $user = Yii::$app->user->id;

        $del = Favorites::find()->where(['user_id' => $user])->andWhere(['sensor_id' => $id])->one()->delete();
        return $this->render('notFavoriteConfirmation');
    }
    
}
