<?php

namespace myCompany\humhub\modules\Sensoren\controllers;

use humhub\modules\admin\components\Controller;
use myCompany\humhub\modules\Sensoren\models\RegisterForm;
use myCompany\humhub\modules\Sensoren\models\Sensor;
use myCompany\humhub\modules\Sensoren\models\Type;
use myCompany\humhub\modules\Sensoren\models\TypeForm;
use myCompany\humhub\modules\Sensoren\models\DeleteForm;
use yii;
use yii\helpers\ArrayHelper;
use myCompany\humhub\modules\Sensoren\models\Unit;

class AdminController extends Controller
{

    /**
     * Render admin only page
     *
     * @return string
     */
    public function actionIndex()
    {
        $sensors = Sensor::find()->all();
        return $this->render('index', ['sensors' => $sensors]);
    }

    public function actionRegister()
    {
            $sensor = new Sensor();
            $model = new RegisterForm();
            
            if($model->load(Yii::$app->request->post()))
            {
                $sensor->name = $model->name;
                $sensor->description = $model->description;
                $sensor->location = $model->location;
                $sensor->unit = $model->unit;
                $sensor->type = $model->type;
                $sensor->sensor_id = Sensor::find()->max('sensor_id') + 1;
                $sensor->remotesensor_id = $model->remotesensor_id;
                

                $type = Type::find()->where(['type_id' => $sensor->type])->one();

                if($sensor->save())
                {
                    Yii::$app->session->setFlash('registerFormSubmitted');
                    return $this->render('registerSensor', ['sensor' => $sensor, 'type' => $type]);
                }
            }
            else
            {
                $items = ArrayHelper::map(Type::find()->all(), 'type_id', 'name');
                return $this->render('registerSensor', ['model' => $model, 'items' => $items]);
            }

        
    }

    public function actionType()
    {
        $model = new TypeForm();
        
        if($model->load(Yii::$app->request->post()))
            {
                $unit = Unit::find()->where(['unit_id' => $model->unit])->one();
                $type = new Type();
                $type->name = $model->name;
                $type->unit_id = $unit->unit_id;
                $type->type_id = Type::find()->max('type_id') + 1;

                if($type->save())
                {
                    Yii::$app->session->setFlash('typeFormSubmitted');
                    return $this->render('registerType', ['type' => $type, 'unit' => $unit]);
                }
            }
            else
            {
                $items = ArrayHelper::map(Unit::find()->all(), 'unit_id', 'name');
                return $this->render('registerType', ['model' => $model, 'items' => $items]);
            }

    }

    public function actionDroptype()
    {
        $model = new DeleteForm();
        if($model->load(Yii::$app->request->post()))
        {
            $type = Type::find()->where(['type_id' => $model->type])->one();
            $deletedType = Type::find()->where(['type_id' => $model->type])->one()->delete();
            Yii::$app->session->setFlash('typedeleted');
            return $this->render('dropType', ['type' => $type]);
        }
        else
        {
            $items = ArrayHelper::map(Type::find()->all(), 'type_id', 'name');
            return $this->render('dropType', ['model' => $model, 'items' => $items]);
        }
    }

    public function actionDeletesensor()
    {
        $id = Yii::$app->request->post('id');
        $sensor = Sensor::find()->where(['sensor_id' => $id])->one();
        $name = $sensor->name;
        $del = Sensor::find()->where(['sensor_id' => $id])->one()->delete();
        return $this->render('deleteConfirmation', ['name' => $name]);
    }

}

