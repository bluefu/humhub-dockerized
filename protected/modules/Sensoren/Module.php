<?php

namespace myCompany\humhub\modules\Sensoren;

use Yii;
use yii\helpers\Url;
use humhub\modules\content\components\ContentContainerActiveRecord;
use humhub\modules\space\models\Space;
use myCompany\humhub\modules\Sensoren\models\Sensor;
use myCompany\humhub\modules\Sensoren\models\Favorites;
use myCompany\humhub\modules\Sensoren\models\Type;
use myCompany\humhub\modules\Sensoren\models\Unit;

class Module extends \humhub\modules\content\components\ContentContainerModule
{
    /**
    * @inheritdoc
    */
    public function getContentContainerTypes()
    {
        return [
            //Space::class,
        ];
    }

    /**
    * @inheritdoc
    */
    public function getConfigUrl()
    {
        return Url::to(['/Sensoren/admin']);
    }

    /**
    * @inheritdoc
    */
    public function disable()
    {
        foreach (Favorites::find()->all() as $entry) {
            $entry->delete();
        }
        foreach (Sensor::find()->all() as $entry) {
            $entry->delete();
        }
        foreach (Type::find()->all() as $entry) {
            $entry->delete();
        }

        // Cleanup all module data, don't remove the parent::disable()!!!
        parent::disable();
    }

    /**
    * @inheritdoc
    */
    public function disableContentContainer(ContentContainerActiveRecord $container)
    {
        // Clean up space related data, don't remove the parent::disable()!!!
        parent::disable();
    }

    /**
    * @inheritdoc
    */
    public function getContentContainerName(ContentContainerActiveRecord $container)
    {
        return Yii::t('SensorenModule.base', 'Sensoren');
    }

    /**
    * @inheritdoc
    */
    public function getContentContainerDescription(ContentContainerActiveRecord $container)
    {
        return Yii::t('SensorenModule.base', 'Hier werden Sensoren und Sensordaten angezeigt.');
    }
}
