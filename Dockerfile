FROM php:7.2-apache

# Install necessary dependencies
RUN apt-get update && apt-get -y upgrade \
    && apt-get -y install libzip-dev zip libpng-dev libjpeg-dev libldap2-dev libfreetype6-dev libicu-dev libpq-dev cron sysv-rc supervisor netcat git \
    && docker-php-ext-configure zip --with-libzip \
    && docker-php-ext-install zip ldap exif intl pdo_mysql pdo_pgsql \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && pecl channel-update pecl.php.net \
    && printf "\n" | pecl install apcu-5.1.5 \
    && docker-php-ext-enable apcu
RUN curl -sS https://getcomposer.org/installer -o composer-setup.php && php composer-setup.php --install-dir=/usr/local/bin --filename=composer

# Enable crontabs and workers with supervisor
COPY docker/crontabs/humhub /etc/cron.d/humhub
RUN crontab /etc/cron.d/humhub
COPY docker/supervisor/humhub.conf /etc/supervisor/conf.d/humhub.conf

# Install Humhub dependencies
WORKDIR /var/www/html
COPY composer.json .
COPY composer.lock .
RUN composer install
COPY . .

# Setup basic scripts for initialization
COPY docker/scripts/start-services /usr/local/bin/
COPY docker/docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT [ "/usr/local/bin/docker-entrypoint.sh" ]