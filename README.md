HumHub - Social Network Kit
===========================

[![Build Status](https://travis-ci.org/humhub/humhub.svg?branch=master)](https://travis-ci.org/humhub/humhub)
[![Yii2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat)](http://www.yiiframework.com/)

HumHub is a feature rich and highly flexible OpenSource Social Network Kit written in PHP.

Important AFTER cloning this project:
* Make sure to have ssh keys on your gitlab.com and gitlab.rz account
* Linux/Mac: `sh init.sh`
* Windows: `init.bat`

How to Docker:
* `docker-compose down -v`: Reset everything (Humhub + Database)
* `docker-compose up`: Start containers (without rebuilding)
* `docker-compose up --build`: Rebuild and start containers
* Clean restart: `docker-compose down -v && docker-compose up --build`

It's perfect for individual:
- Social Intranets
- Enterprise Social Networks
- Private Social Networks

More information:
- [Homepage & Demo](http://www.humhub.org)
- [Documentation & Class Reference](http://docs.humhub.org)
- [Community](http://community.humhub.com/)
- [Licence](http://www.humhub.org/licences)

