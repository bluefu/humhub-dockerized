@echo off

set DIR=%~dp0

:: Initialize submodules
git submodule init
git submodule update

:: Add remote to main humhub project
git remote add main https://github.com/humhub/humhub.git