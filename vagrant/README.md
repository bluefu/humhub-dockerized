# Vagrant Guide for this project

## First of all, install Vagrant
* Installation documentation: [https://www.vagrantup.com/docs/installation/](https://www.vagrantup.com/docs/installation/)

## Install these vagrant plugins
* Hostupdater: `vagrant plugin install vagrant-hostsupdater`
* Puppet-Install: `vagrant plugin install vagrant-puppet-install`
* VBGuest: `vagrant plugin install vagrant-vbguest`
  
## Vagrant commands explained
* Start vagrant VM: `vagrant up` (First time takes a while)
* Access vagrant VM with SSH: `vagrant ssh`
* Stop vagrant VM: `vagrant halt`
* Destroy vagrant VM: `vagrant destroy`

## Guide for first time using `vagrant up`)
1. Start vagrant VM: `vagrant up`
2. Visit [http://localhost:4567](http://localhost:4567)
3. Setup your humhub (Database Configuration => Host:localhost User:humhub Password:password Database:humhub)
4. Access vagrant VM with SSH: `vagrant ssh`
5. Start DB-Listener (**in VM shell**): `humhub-db-listener`

Vagrant installed all HumHub dependencies in the VM **and** the host machine. If you want to start vagrant in the future you just have to execute `vagrant up` and it will use the previously installed version of the VM. This means you can just visit [http://localhost:4567](http://localhost:4567) and everything will be in place.