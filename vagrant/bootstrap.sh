#!/usr/bin/env bash

# Install apache and curl
apt-get update
apt-get install -y apache2 apache2-doc apache2-utils curl git
if ! [ -L /var/www/html ]; then
  rm -rf /var/www/html
  ln -fs /vagrant /var/www/html
fi

# Install database
echo "mysql-server mysql-server/root_password password root" | sudo debconf-set-selections
echo "mysql-server mysql-server/root_password_again password root" | sudo debconf-set-selections
apt-get -y install mysql-server
mysql -uroot -proot -e "CREATE DATABASE humhub DEFAULT CHARACTER SET utf8;"
mysql -uroot -proot -e "CREATE USER humhub@localhost IDENTIFIED BY 'humhub';"
mysql -uroot -proot -e "GRANT ALL PRIVILEGES ON humhub.* TO 'humhub'@'localhost';"
mysql -uroot -proot -e "GRANT ALL PRIVILEGES ON humhub.* TO 'humhub'@'%';"
mysql -uroot -proot -e "FLUSH PRIVILEGES;"

# Install PHP 7
## add dotdeb to apt sources list
echo 'deb http://packages.dotdeb.org jessie all' > /etc/apt/sources.list.d/dotdeb.list

## make sure apt accepts https transport
apt-get -y install apt-transport-https

## add dotdeb key for apt
curl http://www.dotdeb.org/dotdeb.gpg | apt-key add -

## get package list from sources incl. new set source dotdeb
apt-get update

## upgrade to php 7
apt-get -y install php7.0
apt-get -y install php7.0-gd php7.0-json php7.0-mysql php7.0-pdo php7.0-curl php7.0-mbstring php7.0-ldap php7.0-sqlite
apt-get -y install php7.0-mcrypt php7.0-xml php7.0-zip php7.0-intl
apt-get -y install php7.0-dev php-pear
apt-get -y install php7.0-pgsql
pecl channel-update pecl.php.net
printf "\n" | pecl install apcu


# Install composer
curl -sS https://getcomposer.org/installer -o composer-setup.php && php composer-setup.php --install-dir=/usr/local/bin --filename=composer

cd /vagrant
composer install
#chown -R www-data:www-data assets protected/runtime uploads protected/modules protected/config protected/humhub/modules/devtools
chmod +x protected/yii #protected/yii.bat

# Change apache user to vagrant because of file permissions.
sed -i 's/export APACHE_RUN_USER=www-data/#export APACHE_RUN_USER=www-data\nexport APACHE_RUN_USER=vagrant/g' /etc/apache2/envvars

# Setup queue listener script
cat <<EOF >/usr/local/bin/humhub-db-listener
#!/bin/sh

# Start this script to update your db constantly
/usr/bin/php /var/www/html/protected/yii queue/listen
EOF
chmod +x /usr/local/bin/humhub-db-listener

# .env file for development
cat <<EOF >/var/www/html/.env
DISCUSSION_URL="https://ttc-sülzfeld.de"
MEASUREMENTS_LIMIT=1000
CHART_BACKGROUND_COLOR="rgba(0,110,146,0.5)"
CHART_BORDER_COLOR="rgba(179,181,198,1)"
CHART_POINT_BACKGROUND_COLOR="rgba(179,181,198,1)"
CHART_POINT_BORDER_COLOR="#fff"
CHART_POINT_HOVER_BACKGROUND_COLOR="#fff"
CHART_POINT_HOVER_BORDER_COLOR="rgba(179,181,198,1)"
EOF

cd protected
php yii installer/write-db-config "localhost" "humhub" "humhub" "humhub"
php yii installer/install-db
#php yii installer/add-db-config "pgsql" "141.13.162.156" "db_name" "db_user" "db_pass" "bamberg_db"
php yii installer/write-site-config "Test" "humhub@example.de" "de" "Europe/Berlin"
php yii installer/create-admin-account "admin" "admin@example.de" "password"

php yii installer/install-module calendar
php yii module/enable calendar
    
php yii installer/install-module cfiles
php yii module/enable cfiles

php yii installer/install-module polls
php yii module/enable polls

php yii installer/install-module popover-vcard
php yii module/enable popover-vcard

php yii installer/install-module updater
php yii module/enable updater

php yii installer/install-module auto-patch
php yii module/enable auto-patch

php yii module/enable defectreport
php yii module/enable discussion
php yii module/enable Sensoren
php yii search/rebuild

systemctl restart apache2.service
echo 'access http://localhost:4567'
echo 'db user:humhub  password:humhub  db:humhub'